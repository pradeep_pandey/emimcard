package com.mswipe.model;

/**
 *
 * @author bhagyashree
 */
public class CIFCG
{

    String bin;
    int rowid;
    String leadno;
    String custid;
    String mobnr;
    String name; 

    public CIFCG(String bin, int rowid, String leadno, String custid, String mobnr, String name)
    {
        this.bin = bin;
        this.rowid = rowid;
        this.leadno = leadno;
        this.custid = custid;
        this.mobnr = mobnr;
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBin()
    {
        return bin;
    }

    public void setBin(String bin)
    {
        this.bin = bin;
    }

    public int getRowid()
    {
        return rowid;
    }

    public void setRowid(int rowid)
    {
        this.rowid = rowid;
    }

    public String getLeadno()
    {
        return leadno;
    }

    public void setLeadno(String leadno)
    {
        this.leadno = leadno;
    }

    public String getCustid()
    {
        return custid;
    }

    public void setCustid(String custid)
    {
        this.custid = custid;
    }

    public String getMobnr()
    {
        return mobnr;
    }

    public void setMobnr(String mobnr)
    {
        this.mobnr = mobnr;
    }

}
