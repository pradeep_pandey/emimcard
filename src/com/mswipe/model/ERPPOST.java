package com.mswipe.model;

/**
 *
 * @author bhagyashree
 */
public class ERPPOST 
{
     String mcardtype;
        int noofcards;
        String custcode;
        String overdraft;
        String mcarddba;
        String bintype;
        String cardtheme;
        String printlogo;
        String logofilename;
        String mcardlogo;
        String leadno;
        String curraddr1;
        String curraddr2;
        String curraddr3;
        String currstate;
        String currcity;
        String currzipcode;
        String peraddr1;
        String peraddr2;
        String peraddr3;
        String perstate;
        String percity;
        String percountry;
        String perzipcode;
        String emailid;
        String createdby;
        String app_type;

    public String getApp_type()
    {
        return app_type;
    }

    public void setApp_type(String app_type)
    {
        this.app_type = app_type;
    }

    public String getMcardtype() {
        return mcardtype;
    }

    public void setMcardtype(String mcardtype) {
        this.mcardtype = mcardtype;
    }

    public int getNoofcards() {
        return noofcards;
    }

    public void setNoofcards(int noofcards) {
        this.noofcards = noofcards;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(String overdraft) {
        this.overdraft = overdraft;
    }

    public String getMcarddba() {
        return mcarddba;
    }

    public void setMcarddba(String mcarddba) {
        this.mcarddba = mcarddba;
    }

    public String getBintype() {
        return bintype;
    }

    public void setBintype(String bintype) {
        this.bintype = bintype;
    }

    public String getCardtheme() {
        return cardtheme;
    }

    public void setCardtheme(String cardtheme) {
        this.cardtheme = cardtheme;
    }

    public String getPrintlogo() {
        return printlogo;
    }

    public void setPrintlogo(String printlogo) {
        this.printlogo = printlogo;
    }

    public String getLogofilename() {
        return logofilename;
    }

    public void setLogofilename(String logofilename) {
        this.logofilename = logofilename;
    }

    public String getMcardlogo() {
        return mcardlogo;
    }

    public void setMcardlogo(String mcardlogo) {
        this.mcardlogo = mcardlogo;
    }

    public String getLeadno() {
        return leadno;
    }

    public void setLeadno(String leadno) {
        this.leadno = leadno;
    }

    public String getCurraddr1() {
        return curraddr1;
    }

    public void setCurraddr1(String curraddr1) {
        this.curraddr1 = curraddr1;
    }

    public String getCurraddr2() {
        return curraddr2;
    }

    public void setCurraddr2(String curraddr2) {
        this.curraddr2 = curraddr2;
    }

    public String getCurraddr3() {
        return curraddr3;
    }

    public void setCurraddr3(String curraddr3) {
        this.curraddr3 = curraddr3;
    }

    public String getCurrstate() {
        return currstate;
    }

    public void setCurrstate(String currstate) {
        this.currstate = currstate;
    }

    public String getCurrcity() {
        return currcity;
    }

    public void setCurrcity(String currcity) {
        this.currcity = currcity;
    }

    public String getCurrzipcode() {
        return currzipcode;
    }

    public void setCurrzipcode(String currzipcode) {
        this.currzipcode = currzipcode;
    }

    public String getPeraddr1() {
        return peraddr1;
    }

    public void setPeraddr1(String peraddr1) {
        this.peraddr1 = peraddr1;
    }

    public String getPeraddr2() {
        return peraddr2;
    }

    public void setPeraddr2(String peraddr2) {
        this.peraddr2 = peraddr2;
    }

    public String getPeraddr3() {
        return peraddr3;
    }

    public void setPeraddr3(String peraddr3) {
        this.peraddr3 = peraddr3;
    }

    public String getPerstate() {
        return perstate;
    }

    public void setPerstate(String perstate) {
        this.perstate = perstate;
    }

    public String getPercity() {
        return percity;
    }

    public void setPercity(String percity) {
        this.percity = percity;
    }

    public String getPercountry() {
        return percountry;
    }

    public void setPercountry(String percountry) {
        this.percountry = percountry;
    }

    public String getPerzipcode() {
        return perzipcode;
    }

    public void setPerzipcode(String perzipcode) {
        this.perzipcode = perzipcode;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
        
        
}
