/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mswipe.model;

/**
 *
 * @author pradeep
 */
public class ProdDetails
{
    String prd_cvv_key;
    String prd_srv_code;

    public String getPrd_cvv_key()
    {
        return prd_cvv_key;
    }

    public void setPrd_cvv_key(String prd_cvv_key)
    {
        this.prd_cvv_key = prd_cvv_key;
    }

    public String getPrd_srv_code()
    {
        return prd_srv_code;
    }

    public void setPrd_srv_code(String prd_srv_code)
    {
        this.prd_srv_code = prd_srv_code;
    }
    
    
}
