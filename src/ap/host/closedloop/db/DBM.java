package ap.host.closedloop.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.jtds.jdbcx.JtdsDataSource;

public class DBM
{
  private boolean isJdbcDriverRegistered;
  private JtdsDataSource jds;
  final int _INITIAL_DB_CONNS = 1;
  final int _MAX_DB_CONNS = 10;
  final int _LOGIN_TIMEOUT = 1000;
  final int _SOCKET_TIMEOUT = 120000;
  int max_db_conns;
  String filePath;
  
  public DBM()
  {
    this.jds = new JtdsDataSource();
    
    this.max_db_conns = 10;
    this.filePath = null;
    if (!registerdriver())
    {
      System.out.println("Could not register driver");
      this.isJdbcDriverRegistered = false;
    }
    else
    {
      this.isJdbcDriverRegistered = true;
    }
  }
  
  private boolean registerdriver()
  {
//    if (this.filePath == null)
//    {
//      this.filePath = System.getenv("ANTAPP_PATH");
//      if (this.filePath == null)
//      {
//        System.out.println("Could not get ANTAPP_PATH");
//        return false;
//      }
//    }
    Properties dbprop = new Properties();
    try
    {
      this.filePath = "/usr/share/antpay/config/dbconfig.cfg";
      dbprop.load(new FileInputStream(new File(this.filePath)));
      
      this.jds.setAppName("AntPay");
      this.jds.setUser(dbprop.getProperty("UserName"));
      this.jds.setPassword(dbprop.getProperty("Password"));
      this.jds.setServerName(dbprop.getProperty("ServerName"));
      this.jds.setDatabaseName(dbprop.getProperty("DatabaseName"));
      this.jds.setSocketTimeout(Integer.parseInt(dbprop.getProperty("SocketTimeout")));
      this.jds.setLoginTimeout(Integer.parseInt(dbprop.getProperty("LoginTimeout")));
      this.jds.setPortNumber(Integer.parseInt((String)dbprop.get("Port")));
    }
    catch (IOException ex)
    {
      Logger.getLogger(DBM.class.getName()).log(Level.SEVERE, null, ex);
      return false;
    }
    return true;
  }
  
  public Connection getConnection()
    throws SQLException
  {
    synchronized (this)
    {
      if (this.isJdbcDriverRegistered) {
        return this.jds.getConnection();
      }
      Logger.getLogger(DBM.class.getName()).log(Level.SEVERE, "Driver not registered.");
    }
    return null;
  }
}
