package ap.host.closedloop.db;

import ap.host.closedloop.Config;
import ap.host.closedloop.utils.Helper;
import ap.sdk.util.FormatData;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import com.mswipe.model.CIFCG;
import com.mswipe.model.ERPPOST;
import com.mswipe.model.ProdDetails;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author abhi
 */
public class DBHandler
{

    PreparedStatement cs;
    ResultSet rs;
    JSONObject record;
    JSONObject record1;
    String jsonArray = null;

    public void updateemicardsposted(String updatecardslist)
    {
        try
        {
            Connection conn;
            String query = "update cms_cards set emimealpost='1' where cd_card in " + updatecardslist + "";
            conn = Helper.dbm.getConnection();
            cs = conn.prepareStatement(query);
            //cs.setString(1, updatecardslist);
            cs.executeUpdate();

            cs.close();
            conn.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getEMIData(int count, String type)
    {
        boolean found = false;
        String cd_card;
        String firstsix;
        String lastfour;
        String fname;
        String lname;
        String expdate;
        String name;
        String custcode;
        String appno;
        String overdraft;
        String cardtheme;
        String printlogo;
        String gender;
        String emailid;
        String mobilenr;
        String state;
        String country;
        String city;
        String pm_addr;
        String zipcode;
        String dob;
        String datancount = "0;0";
        String lead_no;
        String prospectno;
        String batchno;
        JSONArray record_arr = new JSONArray();
        StringBuilder sb = new StringBuilder();
        try
        {
            Connection conn;
            String query = "{call CMS_GETAZURE_POSTDATA_NEW(?,?)}";
            conn = Helper.dbm.getConnection();
            cs = conn.prepareStatement(query);
            cs.setInt(1, count);
            cs.setString(2, type);
            int batchcount = 0;
            rs = cs.executeQuery();

            while (rs.next())
            {
                found = true;

                record = new JSONObject();

                cd_card = rs.getString("CD_CARD");
                firstsix = rs.getString("FIRSTSIX");
                lastfour = rs.getString("LASTFOUR");
                fname = rs.getString("FNAME");
                lname = rs.getString("LNAME");
                expdate = rs.getString("EXPDATE");
                appno = rs.getString("APPNO");
                overdraft = rs.getString("OVERDRAFT");
                custcode = rs.getString("CUSTCODE");
                cardtheme = rs.getString("CARDTHEME");
                printlogo = rs.getString("PRINTLOGO");
                name = rs.getString("NAME");
                emailid = rs.getString("EMAILID");
                pm_addr = rs.getString("PM_ADDR");
                dob = rs.getString("DOB");
                zipcode = rs.getString("ZIPCODE");
                country = rs.getString("COUNTRY");
                city = rs.getString("CITY");
                state = rs.getString("STATE");
                mobilenr = rs.getString("MOBILENR");
                gender = rs.getString("GENDER");
                lead_no = rs.getString("LEADNO");
                prospectno = rs.getString("PROSPECTNO");
                batchno = rs.getString("ROWID");

                batchcount++;

                String cardno = sha2hash(cd_card);
                String pan = sha2hash(Helper.mcardkey + cd_card);

                record.put("cardno", cardno);
                record.put("firstsix", firstsix);
                record.put("lastfour", lastfour);
                record.put("expdate", expdate);
                record.put("name", name);
                record.put("pan", pan);
                record.put("appno", appno);
                record.put("custcode", custcode);
                record.put("overdraft", overdraft);
                record.put("cardtheme", cardtheme);
                record.put("printlogo", printlogo);
                record.put("gender", gender);
                record.put("emailid", emailid);
                record.put("address", pm_addr);
                record.put("dob", dob);
                record.put("pincode", zipcode);
                record.put("country", country);
                record.put("city", city);
                record.put("state", state);
                record.put("mobileno", mobilenr);
                record.put("type", type);
                record.put("leadmanagementid", lead_no);
                record.put("prospectno", prospectno);
                record.put("batchnumber", batchno);

                record_arr.add(record);
                sb.append("'").append(cd_card).append("'").append(",");
            }
            String updatecardslist = null;
            if (sb.toString().length() > 3)
            {
                updatecardslist = "(" + sb.substring(0, sb.toString().length() - 1) + ")";
            }
            jsonArray = record_arr.toJSONString();

            datancount = jsonArray + ";" + batchcount + ";" + updatecardslist;
            if (batchcount == 0)
            {
                datancount = null;

            }
            rs.close();
            cs.close();
            conn.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datancount;
    }

    public String sha2hash(String verify) throws NoSuchAlgorithmException
    {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] passBytes = verify.getBytes();
        byte[] passHash = sha256.digest(passBytes);
        return Translate.fromBinToHex(Translate.getString(passHash));
    }

    public void addmcards(String ncard, String expirydate, String custcode, String appno, String cardtheme, String printlogo, String enchashpan, CIFCG cifcg)
    {
        try
        {

            // Add in cms_cards, cms_card_plastic, cms_balances, cms_accounts
            String query = "{call usp_add_closeloopcard_bin_new(?,?,?,?,?,?,?,?,?,?)}";
            try (Connection conn = Helper.dbm.getConnection())
            {
                cs = conn.prepareCall(query);
                cs.setString(1, ncard);
                cs.setString(2, expirydate);
                //String issure_node = "IEMIMCARD";
                //int bank_ID = getBankID(issure_node);
                cs.setInt(3, 11); // Need to ask with Sir
                cs.setString(4, custcode);
                String overdraft = "N";
                cs.setLong(5, cifcg.getRowid());
                overdraft = "N";

                cs.setString(6, appno);
                cs.setString(7, overdraft);
                cs.setString(8, cardtheme);
                cs.setString(9, printlogo);
                cs.setString(10, enchashpan);
                cs.execute();
                cs.close();
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<CIFCG> getCIFRequestData()
    {
        int batchcount = 0;
        boolean found = false;
        JSONArray record_arr = new JSONArray();
        StringBuilder sb = new StringBuilder();
        List<CIFCG> ciflist = new ArrayList<CIFCG>();
        try
        {
            Connection conn;
            String query = "select bin,rowid,Lead_No,mob_nr,cust_id,fname,lname from cif(nolock) where maction = '0'";
            conn = Helper.dbm.getConnection();
            cs = conn.prepareStatement(query);
            rs = cs.executeQuery();

            while (rs.next())
            {
                found = true;
                ciflist.add(new CIFCG(rs.getString("bin"), rs.getInt("rowid"), rs.getString("Lead_No"), rs.getString("cust_id"), rs.getString("mob_nr"), rs.getString("fname") + " " + rs.getString("lname")));
                batchcount++;
            }

            if (batchcount == 0)
            {
                ciflist = null;

            }
            rs.close();
            cs.close();
            conn.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ciflist;
    }

    public void updateerpposted(String updatecardslist)
    {
        try
        {
            Connection conn;
            String query = "update cms_cards set emierppost='1' where cd_card in " + updatecardslist + "";
            conn = Helper.dbm.getConnection();
            cs = conn.prepareStatement(query);
            cs.executeUpdate();

            cs.close();
            conn.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ProdDetails getProductDetails(String bin)
    {
        ProdDetails proDetails = null;

        String query = "SELECT prd_cvv_key,prd_srv_code from cms_product(nolock) where prd_bin = '" + bin + "'";
        Connection conn;
        PreparedStatement ps;
        ResultSet rs;
        try
        {
            conn = Helper.dbm.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next())
            {
                proDetails = new ProdDetails();

                proDetails.setPrd_cvv_key(rs.getString("prd_cvv_key").trim());
                proDetails.setPrd_srv_code(rs.getString("prd_srv_code").trim());
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ae)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ae);
            proDetails = null;
        }

        return proDetails;
    }

    public String getMaxCard(String type)
    {
        boolean found = false;
        String maxCard = "";
        try
        {
            Connection conn;
            String query = "{call CMS_MCARD_MAXCARD(?)}";
            conn = Helper.dbm.getConnection();
            cs = conn.prepareStatement(query);
            cs.setString(1, type);
            rs = cs.executeQuery();

            while (rs.next())
            {
                found = true;
                maxCard = rs.getString(1);
            }

            rs.close();
            cs.close();
            conn.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maxCard;
    }

    public static int addCardBatch(int bankId, String batch_type, int count, String status, String overdraft, String disc_data)
    {
        String query = "insert into cms_batch("
                + "cb_bank_id,cb_batch_type,cb_count,"
                + "cb_status,cb_last_upd_datetime,cb_last_upd_user,"
                + "cb_disc_data,overdraft)"
                + "values(?,?,?,?,getdate(),?,?,?)";
        Connection conn;
        PreparedStatement ps;
        ResultSet rs;
        CallableStatement cs;
        int batch_nr = -1;

        try
        {
            conn = Config.dbm.getConnection();
            //  ps = conn.prepareStatement("select nextval('cms_batch_nr');");
            ps = conn.prepareStatement("select NEXT VALUE for cms_batch_nr;");
            rs = ps.executeQuery();
            while (rs.next())
            {
                batch_nr = rs.getInt(1);
            }
            rs.close();
            ps.close();

            ps = conn.prepareStatement(query);
            ps.setInt(1, bankId);
            ps.setString(2, batch_type);
            ps.setInt(3, count);
            ps.setString(4, status);
            ps.setString(5, "AntPay");
            ps.setString(6, disc_data);
            ps.setString(7, overdraft);

            ps.executeUpdate();
            ps.close();

//            cs = conn.prepareCall(query);
//            cs.setInt(1, bankId);
//            cs.setString(2, batch_type);
//            cs.setInt(3, count);
//            cs.setString(4, status);
//            cs.setString(5, "AntPay");
//            cs.setString(6, disc_data);
//            cs.setInt(7, batch_nr);
//
//            cs.execute();
//            cs.close();
            conn.close();
        } catch (SQLException ex)
        {
            Config.logger.error("Error in #addCardBatch");
            Config.logger.error(ex);
        }

        return batch_nr;
    }

    public static JSONObject addNCardBatch(int bankId, String batch_type, int count, String status, String overdraft, String disc_data, String bincode, String cust_code, String printlogo, String remark)
    {
        String query = "insert into cms_batch("
                + "cb_bank_id,cb_batch_type,cb_count,"
                + "cb_status,cb_last_upd_datetime,cb_last_upd_user,"
                + "cb_disc_data,overdraft,printlogo,custcode,remark)"
                + "values(?,?,?,?,getdate(),?,?,?,?,?,?)";
        Connection conn;
        PreparedStatement ps;
        ResultSet rs;
        CallableStatement cs;
        int batch_nr = -1;
        JSONObject jobj2 = new JSONObject();

        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareStatement("select NEXT VALUE for cms_batch_nr;");
            rs = ps.executeQuery();
            while (rs.next())
            {
                batch_nr = rs.getInt(1);
            }
            rs.close();
            ps.close();

            ps = conn.prepareStatement(query);
            ps.setInt(1, bankId);
            ps.setString(2, batch_type);
            ps.setInt(3, count);
            ps.setString(4, status);
            ps.setString(5, "AntPay");
            ps.setString(6, disc_data);
            ps.setString(7, overdraft);
            ps.setString(8, printlogo);
            ps.setString(9, cust_code);
            ps.setString(10, remark);
            int updcount = ps.executeUpdate();
            ps.close();
            conn.close();
            if (updcount > 0)
            {

                jobj2.put("status", true);

                if (bincode == null || bincode.equals(""))
                {
                    jobj2.put("status", false);
                }

            } else
            {
                jobj2.put("status", false);
            }

            jobj2.put("bintype", bincode);
            jobj2.put("batchnr", batch_nr);
        } catch (SQLException ex)
        {
            Config.logger.error("Error in #addCardBatch");
            Config.logger.error(ex);
        }

        return jobj2;
    }

    /**
     *
     * @param custCode
     * @return
     */
    public static String getCustomerCardCountAmount(String custCode)
    {
        long amount = 0;
        long count = 0;
        String data = "0;0";

        Connection conn;
        PreparedStatement ps;
        ResultSet rs;

        String query;

        query = "select count(*) from cms_cards(nolock) where priv_field_1='" + custCode + "';";

        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next())
            {
                count = rs.getLong(1);
            }

            data = Long.toString(count) + ";";

            query = "select sum(bal_avail_bal) from cms_balances(nolock) where bal_account_nr in "
                    + "(select cd_card from cms_cards(nolock)  where cd_status='A' and priv_field_1='" + custCode + "');";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next())
            {
                amount = rs.getLong(1);
            }

            data = data + Long.toString(amount);

            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex)
        {
            Config.logger.error("Error in #getCardCount");
            Config.logger.error(ex);
        }

        return data;
    }

    public static JSONArray getRegisteredCards(String custCode)
    {
        int activecards = 0;
        int inactivecards = 0;

        String issue_date;
        boolean found = false;

        JSONObject record = new JSONObject();
        JSONArray record_arr = new JSONArray();
        String data = "0;0";

        Connection conn;
        //CallableStatement cs;
        PreparedStatement cs;
        ResultSet rs;
        StringBuilder sb = new StringBuilder();
        String query;

        query = "{call usp_get_regcards(?)}";

        try
        {
            conn = Config.dbm.getConnection();
            // cs = conn.prepareCall(query);
            cs = conn.prepareStatement(query);
            cs.setString(1, custCode);
            rs = cs.executeQuery();

            while (rs.next())
            {
                found = true;

                record = new JSONObject();

                activecards = rs.getInt("activecards");
                inactivecards = rs.getInt("inactivecards");

                record.put("ActiveCards", activecards);
                record.put("InActiveCards", inactivecards);
                record.put("CustCode", custCode);

                record_arr.add(record);
            }

            rs.close();
            cs.close();
            conn.close();

            if (found == true)
            {
                // data = record_arr.toJSONString().replaceAll("\\\\", "");
                data = record.toJSONString();
            } else
            {
                record_arr.add("NOK");
                data = "NOK";
            }
        } catch (Exception ex)
        {
            Config.logger.error("Error in #getBalanceSummaryDetails");
            Config.logger.error(ex);
            record_arr.add("NOK");
            data = "NOK";
        }

        return record_arr;
    }

    public static String getRegisteredCards1(String custCode)
    {
        int activecards = 0;
        int inactivecards = 0;
        String data = "0;0";

        Connection conn;
        PreparedStatement ps;
        ResultSet rs;

        String query;

        query = "{call usp_get_regcards(?)}";

        try
        {
            conn = Config.dbm.getConnection();
            // cs = conn.prepareCall(query);
            ps = conn.prepareStatement(query);
            ps.setString(1, custCode);
            rs = ps.executeQuery();
            while (rs.next())
            {
                activecards = rs.getInt("activecards");
                inactivecards = rs.getInt("inactivecards");
            }

            data = activecards + ";" + inactivecards;

            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex)
        {
            Config.logger.error("Error in #getCardCount");
            Config.logger.error(ex);
        }

        return data;
    }

    /**
     *
     * @param custCode
     * @return
     */
    public JSONObject getEMIERPData(String ncard)
    {
        boolean found = false;
        JSONObject jobj = new JSONObject();
        ERPPOST erppost = null;
        record = new JSONObject();
        record1 = new JSONObject();
        JSONArray record_arr = new JSONArray();
        JSONArray record_arr_1 = new JSONArray();
        StringBuilder sb = new StringBuilder();
        try
        {
            Connection conn;
            String query = "{call CMS_EMIERP_POST(?)}";
            conn = Helper.dbm.getConnection();
            cs = conn.prepareStatement(query);
            cs.setString(1, ncard);
            int batchcount = 0;
            rs = cs.executeQuery();

            while (rs.next())
            {
                erppost = new ERPPOST();
                found = true;

                erppost.setMcardtype(rs.getString("mcardtype"));
                erppost.setNoofcards(rs.getInt("noofcards"));
                erppost.setCustcode(rs.getString("custcode"));
                erppost.setOverdraft(rs.getString("overdraft"));
                erppost.setMcarddba(rs.getString("mcarddba"));
                erppost.setBintype(rs.getString("bintype"));
                erppost.setCardtheme(rs.getString("cardtheme"));
                erppost.setPrintlogo(rs.getString("printlogo"));
                erppost.setLogofilename(rs.getString("logofilename"));
                erppost.setMcarddba(rs.getString("mcardlogo"));
                erppost.setLeadno(rs.getString("leadno"));
                erppost.setCurraddr1(rs.getString("curr_addr_1"));
                erppost.setCurraddr2(rs.getString("curr_addr_2"));
                erppost.setCurraddr3(rs.getString("curr_addr_3"));
                erppost.setCurrcity(rs.getString("curr_city"));
                erppost.setCurrstate(rs.getString("curr_state"));
                erppost.setCurrzipcode(rs.getString("cm_zipcode"));
                erppost.setPeraddr1(rs.getString("per_addr1"));
                erppost.setPeraddr2(rs.getString("per_addr2"));
                erppost.setPeraddr3(rs.getString("per_addr3"));
                erppost.setPercity(rs.getString("per_city"));
                erppost.setPerstate(rs.getString("per_state"));
                erppost.setPercountry(rs.getString("per_country"));
                erppost.setPerzipcode(rs.getString("per_zipcode"));
                erppost.setEmailid(rs.getString("emailid"));
                erppost.setCreatedby(rs.getString("createdby"));
                erppost.setApp_type(rs.getString("app_type"));

                record.put("Appl_No", erppost.getLeadno());
                record.put("McardType", erppost.getMcardtype());
                record.put("NoOfCards", erppost.getNoofcards());
                record.put("MCardDBA", erppost.getMcarddba());

                record.put("LogoFileName", erppost.getLogofilename());
                record.put("McardLogo", "");
                //record.put("Overdraft", Boolean.parseBoolean("True"));
                record.put("Location_Code", 0);

                record.put("BinBatchCode", erppost.getBintype());
                record.put("Card_Theme", erppost.getCardtheme());
                record.put("Cust_Code", erppost.getCustcode());
                record.put("Created_By", erppost.getCreatedby());
                record.put("App_Type", erppost.getApp_type());

                record1.put("Lead_No", erppost.getLeadno());
                record1.put("Curr_Addr_1", erppost.getCurraddr1());
                record1.put("Curr_Addr_2", erppost.getCurraddr2());
                record1.put("Curr_Addr_3", erppost.getCurraddr3());
                record1.put("Curr_State", erppost.getCurrstate());
                record1.put("Curr_city", erppost.getCurrcity());
                record1.put("Curr_pincode", erppost.getCurrzipcode());
                record1.put("Per_Addr_1", erppost.getPeraddr1());
                record1.put("Per_Addr_2", erppost.getPeraddr2());
                record1.put("Per_Addr_3", erppost.getPeraddr3());
                record1.put("Per_State", erppost.getPerstate());
                record1.put("Per_city", erppost.getPercity());
                record1.put("Per_pincode", erppost.getPerzipcode());
                record1.put("Preferred_Mailing_Address", erppost.getEmailid());
                record1.put("Country", erppost.getPercountry());
                record1.put("Created_By", erppost.getCreatedby());

                record_arr.add(record);
                record_arr_1.add(record1);

                batchcount++;

            }
            jobj.put("McardDetailstype", record_arr);
            jobj.put("Addresstype", record_arr_1);

            //System.out.println(jobj.toJSONString());

            rs.close();
            cs.close();
            conn.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jobj;
    }

    public static JSONArray getBalanceSummaryDetails(String custCode)
    {
        String pan;
        String cust_name;
        long balance;
        String issue_date;
        boolean found = false;

        JSONObject record = new JSONObject();
        JSONArray record_arr = new JSONArray();
        String data = "0;0";

        Connection conn;
        //CallableStatement cs;
        PreparedStatement cs;
        ResultSet rs;
        StringBuilder sb = new StringBuilder();
        String query;

        query = "{call cms_cardbalance(?)}";

        try
        {
            conn = Config.dbm.getConnection();
            // cs = conn.prepareCall(query);
            cs = conn.prepareStatement(query);
            cs.setString(1, custCode);
            rs = cs.executeQuery();

            while (rs.next())
            {
                found = true;

                record = new JSONObject();

                pan = rs.getString("cd_card");
                if (null == pan)
                {
                    pan = "xxxxxx";
                } else
                {
                    pan = FormatData.protect(pan);
                }

                cust_name = rs.getString("cs_first_name");
                if (null == cust_name)
                {
                    cust_name = "NA";
                }

                balance = rs.getLong("bal_avail_bal");

                issue_date = rs.getString("cd_reg_datetime");
                if (null == issue_date)
                {
                    issue_date = "NA";
                } else
                {

                    issue_date = issue_date.substring(0, 19);
                }
                String availAmt;
                if (balance >= 0)
                {
                    availAmt = Utility.formatAmount(String.valueOf(balance));
                } else
                {
                    availAmt = "-" + Utility.formatAmount(String.valueOf(balance).replaceAll("-", ""));
                }

                record.put("PAN", pan);
                record.put("CustName", cust_name);
                //record.put("Balance",Utility.formatAmount(Long.toString(balance)));
                record.put("Balance", availAmt);
                record.put("IssueDate", issue_date);

                record_arr.add(record);
            }

            rs.close();
            cs.close();
            conn.close();

            if (found == true)
            {
                // data = record_arr.toJSONString().replaceAll("\\\\", "");
                data = record.toJSONString();
            } else
            {
                record_arr.add("NOK");
                data = "NOK";
            }
        } catch (Exception ex)
        {
            Config.logger.error("Error in #getBalanceSummaryDetails");
            Config.logger.error(ex);
            record_arr.add("NOK");
            data = "NOK";
        }

        return record_arr;
    }

//    public static int updateAppNrtoBulkCards(String appNo,String custcode)
//    {
//        int count = 0;
//        Connection conn = null;
//        CallableStatement cs;
//
//        try {
//            conn = Config.dbm.getConnection();
//            Config.logger.info("appnr - "+appNo+ ",custcode - "+custcode);
//            //cs = conn.prepareCall("{call cms_updatecard(?,?)}");
//            cs = conn.prepareCall("update cms_cards set cd_app_no= ? where priv_field_1=?");
//            cs.setString(1, appNo);
//            cs.setString(2, custcode);
//             cs.executeUpdate();
//            Config.logger.info("Count - "+count);
//            cs.close();
//            conn.close();
//        } catch (SQLException ex) {
//            Config.logger.error(ex);
//        }
//
//        return count;
//    }
    public static int updateAppNrtoCards(String appNo, String custcode)
    {
        Connection conn = null;
        boolean flag = false;
        CallableStatement cs;
        int count = 0;
        try
        {
            conn = Config.dbm.getConnection();

            cs = conn.prepareCall("{call cms_updatecard(?,?)}");

            cs.setString(1, appNo);
            cs.setString(2, custcode);
            cs.execute();

            cs.close();
            conn.close();
            flag = true;
        } catch (SQLException ex)
        {
            // ex.printStackTrace();
            Config.logger.error(ex);
        }

        return count;
    }

    public static boolean isValidCardData(String cardnr, String dbcustcode)
    {
        boolean flag = false;
        Connection conn;
        PreparedStatement ps;
        ResultSet rs;
        String query;
        query = "select cd_card,cd_status,priv_field_1 from cms_cards where cd_card='" + cardnr + "' and priv_field_1='" + dbcustcode + "';";
        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next())
            {
                flag = true;
            }
            ps.close();
            rs.close();
            conn.close();
        } catch (SQLException ex)
        {
            flag = false;
            Config.logger.error("Error in #getIsValidCustCode");
            Config.logger.error(ex);
        }

        return flag;
    }

    public void updateemibatchnr(int batchnr, String pan)
    {
        try
        {
            Connection conn;
            String upd = "update cif set maction= '1',pan='" + pan + "' where rowid=" + batchnr + "";
            conn = Helper.dbm.getConnection();
            try (PreparedStatement ps = conn.prepareStatement(upd))
            {
                ps.executeUpdate();
            }
            conn.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private int getBankID(String issue_node)
    {
        int data = 1;

        Connection conn;
        PreparedStatement ps;
        ResultSet rs;

        String query;

        query = "select part_id from cms_issuer where issuer_node = ?";

        try
        {
            conn = Config.dbm.getConnection();
            // cs = conn.prepareCall(query);
            ps = conn.prepareStatement(query);
            ps.setString(1, issue_node);
            rs = ps.executeQuery();
            while (rs.next())
            {
                data = rs.getInt("part_id");
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (Exception ex)
        {
            Config.logger.error("Error in #getCardCount");
            Config.logger.error(ex);
        }

        return data;
    }
}
