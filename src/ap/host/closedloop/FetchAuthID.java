package ap.host.closedloop;

import ap.sdk.util.Translate;

/**
 *
 * @author abhishek1.m
 */
public class FetchAuthID
{

    long authid;
    long rrn;

    public FetchAuthID()
    {
        authid = 0;
        rrn = 0;
    }
    
    /**
     * 
     * @return 
     */
    public synchronized String getNextAuthID()
    {
        String str_authid;
        ++authid;
        
        if(authid>999999)
        {
            authid=1;
        }
        
        str_authid=Translate.resize(Long.toString(authid), 6, '0', false);
        
        return str_authid;
    }
    
    public synchronized String getNextRRN()
    {
        String str_rrn;
        ++rrn;
        
        str_rrn=Translate.resize(Long.toString(rrn), 12, '0', false);
        
        return str_rrn;
    }
}
