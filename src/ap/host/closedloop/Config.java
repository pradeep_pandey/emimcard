package ap.host.closedloop;

import ap.host.closedloop.utils.Helper;
import static ap.host.closedloop.utils.Helper.prop;
import ap.sdk.jdbc.DBManager;

import ap.sdk.jdbc.DBManager;
import ap.sdk.util.FetchProperty;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author abhishek1.m
 */
public class Config {

    public static String _MODULE_NAME = "EMIMCard";
    public static Logger logger;
    public static DBManager dbm;
    public static Random rand;
    public static ConcurrentHashMap<String, String> hm_data;
    public static ConcurrentHashMap<String, Long> hm_otp;
    public static ConcurrentHashMap<String, Long> hm_data_timer;
 //   public static ConcurrentHashMap<String, String> hm_rsp;
    public static String smsGateway;
    //public static String balanceInq;
    public static String emisale;
    public static String saltvalue;
    //public static String topup;
    public static FetchAuthID fai;
    public static FetchAuthID rrn;
    //public static Properties prop = new Properties();

    public Config() {
    }

    /**
     *
     */
    public static void initializeConfigurations() {
        dbm = new DBManager();
        //prop = loadConfigProperties();
        logger = LogManager.getLogger(_MODULE_NAME);

        rand = new Random();

        hm_data = new ConcurrentHashMap<>();
        hm_otp = new ConcurrentHashMap<>();
        hm_data_timer = new ConcurrentHashMap<>();
        
//        hm_rsp = new ConcurrentHashMap<>();
//        hm_rsp.put("201", "00");
//        hm_rsp.put("401", "22");
//        hm_rsp.put("403", "07");
//        hm_rsp.put("400", "03");
//        hm_rsp.put("500", "96");
//        hm_rsp.put("404", "25");
//        

        smsGateway = FetchProperty.getValue("SMSGatewayURL");
        if (null == smsGateway) {
            smsGateway = "NA";
        }
        saltvalue = FetchProperty.getValue("cardbatchkey");
        //balanceInq = FetchProperty.getValue("EMIBalInq");//MCardBalInq
        emisale = FetchProperty.getValue("MCardEmiSale");
        //topup = FetchProperty.getValue("MCardTopup");

        fai = new FetchAuthID();
        rrn = new FetchAuthID();
    }
    
//    public static Properties loadConfigProperties() {
//        InputStream input = null;
//        try {
//            input = new FileInputStream("/usr/share/antpay/config/MCardPin_Config.properties");
//
//	// load a properties file
//            prop.load(input);
//        } catch (IOException ex) {
//            System.out.println("Unable to load config file - " + ex.getMessage());
//        }
//        return prop;
//    }

    
}
