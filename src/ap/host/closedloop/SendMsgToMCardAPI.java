package ap.host.closedloop;

import ap.host.closedloop.utils.Helper;
import ap.sdk.util.Translate;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author bhagyashree
 */
public class SendMsgToMCardAPI {
    
    String url;
    String data;
    String header;
    String response;
    
    public SendMsgToMCardAPI(String url, String data, String header)
    {
        this.url = url;
        this.data = data;
        this.header = header;
    }
    
    
    public String postdata()
    {
        try {

            URL url;
            URLConnection uconn;
            int timeout = 50000;

            DataOutputStream dos;
            DataInputStream dis;

            url = new URL(this.url);
            uconn = url.openConnection();
            uconn.setRequestProperty("key", this.header);
            uconn.setRequestProperty("Content-Type", "application/json");
            uconn.setRequestProperty("charset", "utf-8");
            uconn.setReadTimeout(timeout);
            uconn.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(uconn.getOutputStream());
            out.write(this.data);
            out.close();

            dis = new DataInputStream(uconn.getInputStream());

            StringBuilder str = new StringBuilder();
            int x;
            while ((x = dis.read()) != -1) {
                str.append((char) x);
            }

            response = str.toString();
            System.out.println("Response - "+response);
            dis.close();
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException - " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("IOException - " + ex.getMessage());
        } 
        return response;
    }
    
     
}
