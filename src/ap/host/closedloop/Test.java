/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.host.closedloop;

import static ap.host.closedloop.ProcessMsg.Sha2Pin;
import ap.host.closedloop.utils.Helper;
import ap.sdk.crypto.HSMKeyMgmt;
import ap.sdk.jdbc.DBManager;
import ap.sdk.message.Iso8583JSON;
import ap.sdk.util.FormatData;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/**
 *
 * @author pradeep
 */
public class Test
{

    public static void main(String[] args) throws SQLException, Exception
    {
        Test Test = new Test();
        String enc_pan = Test.sha2hash("9040010010059477");
        System.out.println(enc_pan);
//        Test Test = new Test();
//        String enc_pan = Test.encrypt("9040010010059477","2300");
//        System.out.println(enc_pan);
//        
//        String pb_org = Sha2Pin("7AAAEC9B73B3FC38");
//        System.out.println(pb_org);

//        String f_35 = "9040010010059477D2308720267";
//        
//        
//        String c_no = f_35.substring(0, 16);
//                    String exp_Date = f_35.substring(17, 21);
//                    String svc_Code = f_35.substring(21, 24);
//                    String cvv = f_35.substring(24, 27);
//                    
//        System.out.println(c_no);
//        System.out.println(exp_Date);
//        System.out.println(svc_Code);
//        System.out.println(cvv);
//        System.out.println(f_35.length());
//        
//        //CF4C163AA8B05F62
//        
//        String pb_org = Sha2Pin("CF4C163AA8B05F62");
//        System.out.println(pb_org);
        //Helper.loadConfigProperties("mcardgen.properties");
//        HSMKeyMgmt hk = new HSMKeyMgmt(new DBManager());
//        String rc = hk.verifyCVV("IIFL_CVK", "9040010010057448", "2307", "720", "385");
//        System.out.println("rc ->" + rc);
//        String name_loc = "TEST                   MUMBAI       MHIN";
//        String name = name_loc.substring(0, 23).trim();
//        System.out.println(name + "--");
//        Calendar cal = Calendar.getInstance(); 
//        String ddd = Utility.resize(Integer.toString(cal.get(6)), 3, "0", false);
//        
//        //String aa = Utility.resize(Long.toString(Long.parseLong(""+dayOfYear)),3, "0", false);
//        //System.out.println(ddd);
//        
//        String ext_7 = "000000106085";
//        String rrn = ext_7.substring(ext_7.length()-3, ext_7.length());
//        
//        String temp = "000000"+ddd+rrn;
//        
//        System.out.println(temp);
    }

    public String sha2hash(String verify) throws NoSuchAlgorithmException
    {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] passBytes = verify.getBytes();
        byte[] passHash = sha256.digest(passBytes);
        return Translate.fromBinToHex(Translate.getString(passHash));
    }
    
    public static String Sha2Pin(String pb) throws Exception
    {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        String pb_org = Translate.fromBinToHex(Translate.getString(digest.digest(Translate.getData(Translate.fromHexToBin(pb)))));
        return pb_org;
    }

    public static String encrypt(String pan, String pin) throws Exception
    {
        String pb = "";
        String pin_pan = pan.substring(pan.length() - 1 - 12, pan.length() - 1);
        pin_pan = Utility.resize(pin_pan, 16, "0", false);
        byte[] b_pan = Translate.getData(Translate.fromHexToBin(pin_pan));
        pin = Utility.resize(Integer.toString(pin.length()), 2, "0", false) + pin;
        pin = Utility.resize(pin, 16, "F", true);
        byte[] pin_pb = Translate.getData(Translate.fromHexToBin(pin));
        byte[] pin_pb_xored = FormatData.xor(b_pan, pin_pb);
        pb = encryptECB(pin_pb_xored, "55eeb4af42f648deca95bdafccb17e8855eeb4af42f648deca95bdafccb17e88");
        pb = pb.substring(0, 16);
        System.out.println(pb);
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        pb = Translate.fromBinToHex(Translate.getString(digest.digest(Translate.getData(Translate.fromHexToBin(pb)))));
        // System.out.println(pb.length());
        return pb;
    }

    public static String encryptECB(byte[] inpBytes, String clearKey)
    {
        byte[] finalCipher = null;
        try
        {
            String algorithm = "DESede";
            String transformation = "DESede/ECB/PKCS5Padding";
            byte[] keyvalue = Translate.getData(Translate.fromHexToBin(clearKey));
            DESedeKeySpec keySpec = new DESedeKeySpec(keyvalue);
            SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
            Cipher encrypter = Cipher.getInstance(transformation);
            encrypter.init(Cipher.ENCRYPT_MODE, key);
            finalCipher = encrypter.doFinal(inpBytes);
        } catch (Exception ex)
        {
            //resp.append(ex.getMessage());
            return Translate.fromBinToHex(Translate.getString(finalCipher));
        }

        return Translate.fromBinToHex(Translate.getString(finalCipher));
    }
}
