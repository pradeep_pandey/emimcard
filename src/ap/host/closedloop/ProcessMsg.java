package ap.host.closedloop;

import ap.host.closedloop.db.DBHandler;
import ap.host.closedloop.utils.Helper;
import ap.sdk.crypto.HSMKeyMgmt;
import ap.sdk.jdbc.DBManager;
import ap.sdk.message.AdditionalAmount;
import ap.sdk.message.Iso8583JSON;
import ap.sdk.message.KeyValuePair;
import ap.sdk.message.iso8583.Iso8583;
import ap.sdk.util.FormatData;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xlightweb.IHttpExchange;

/**
 *
 * @author Abhishek1.M
 */
public class ProcessMsg implements Runnable
{

    public IHttpExchange ihe;
    public String data;

    /**
     *
     * @param ihe
     * @param data
     */
    public ProcessMsg(IHttpExchange ihe, String data)
    {
        this.ihe = ihe;
        this.data = data;
    }

    /**
     *
     */
    @Override
    public void run()
    {
        processMsg();
    }

    /**
     *
     * @param data
     * @return
     */
    private boolean processMsg()
    {
        String uri;
        uri = ihe.getRequest().getRequestURI();

        if (null == uri)
        {
            Utility.sendIHEResponse(ihe, "Invalid URI");

            return false;
        }

        Config.logger.trace("URI = " + uri);

        switch (uri)
        {
            case "/transaction":
                return processTransaction();

            case "/balanceinquiry":
                return balanceinquiry();

            case "/balancesummary":
                return balancesummary();

            case "/balancedetails":
                return clbalancedetails();

            case "/verifyotp":
                return verifyOTP();

            case "/resendotp":
                return resendOTP();

            default:
                return invalidURI();
        }
    }

    /**
     *
     * @return
     */
    private boolean invalidURI()
    {
        JSONObject jobj = new JSONObject();

        jobj.put("ErrorDesc", "Invalid URI");
        jobj.put("status", false);

        Config.logger.trace(jobj.toString());

        ihe.sendError(404);
        Utility.sendIHEResponse(ihe, jobj.toString());

        return true;
    }

    /**
     *
     * @return
     */
    private boolean registration()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj;
        JSONObject jobj2 = new JSONObject();
        long otp = Config.rand.nextInt(8999) + 1000;
        //
        String mobilenr;

        try
        {
            Config.logger.trace("Data\n" + data);
            jobj = (JSONObject) jp.parse(data);

            JSONObject data1_o = (JSONObject) jobj.get("data");

            jobj2.put("OTP", otp);
            jobj2.put("status", true);

            mobilenr = (String) data1_o.get("Mobile");
            Config.hm_otp.put(mobilenr, otp);
            Config.hm_data.put(mobilenr, data);
            Config.hm_data_timer.put(mobilenr, System.currentTimeMillis());

            Thread th = new Thread(()
                    -> 
                    {
                        sendSMS((String) data1_o.get("Mobile"), Long.toString(otp));
            });
            th.start();

            Config.logger.trace(jobj.toString());
        } catch (ParseException ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error parsing data");

            return false;
        }

        Config.logger.trace(jobj2.toString());

        Utility.sendIHEResponse(ihe, jobj2.toString());

        return true;
    }

    /**
     *
     * @return
     */
    private boolean balanceinquiry()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj;
        JSONObject jobjrsp = new JSONObject();

        try
        {
            jobj = (JSONObject) jp.parse(data);

            Config.logger.trace(jobj.toString());
        } catch (ParseException ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error parsing data");

            return false;
        }

        JSONObject data1_o = (JSONObject) jobj.get("data");
        String cardnr = (String) data1_o.get("CardNo");
        String dbcustcode = jobj.get("DBcustcode").toString();
        Card card_db = getCardInfoBal(cardnr, "ICLOSEDLOOP", dbcustcode);
        long newBal = 0;
        if (dbcustcode != null)
        {
            if (card_db != null)
            {
                String balanceAmount;
                if (card_db.getBalanceAmount() >= 0)
                {
                    if (String.valueOf(card_db.getBalanceAmount()).length() == 1)
                    {
                        newBal = card_db.getBalanceAmount() * 100;

                    }
                    newBal = card_db.getBalanceAmount();
                    balanceAmount = Utility.formatAmount(String.valueOf(newBal));
                } else
                {
                    System.out.println(String.valueOf(card_db.getBalanceAmount()).length());
                    if (String.valueOf(card_db.getBalanceAmount()).length() == 2)
                    {
                        newBal = card_db.getBalanceAmount() * 100;
                    }
                    newBal = card_db.getBalanceAmount();
                    //  balanceAmount = "-" + Utility.formatAmount(String.valueOf(card_db.getBalanceAmount()).replaceAll("-", ""));
                    balanceAmount = "-" + Utility.formatAmount(String.valueOf(newBal).replaceAll("-", ""));
                }

                jobj.put("balanceamount", balanceAmount);
                jobj.put("status", true);
                Config.logger.trace(jobj.toString());

                Utility.sendIHEResponse(ihe, jobj.toString());
            } else
            {
                jobjrsp.put("status", false);
                jobjrsp.put("data", "this card does not belong to you");
                Config.logger.trace(jobjrsp.toString());

                Utility.sendIHEResponse(ihe, jobjrsp.toString());
            }
        } else if (dbcustcode.equals(""))
        {
            jobjrsp.put("status", true);
            jobjrsp.put("data", "custcode received blank in request");
            Config.logger.trace(jobjrsp.toString());

            Utility.sendIHEResponse(ihe, jobjrsp.toString());
        }

//        Config.logger.trace(jobj.toString());
//
//        Utility.sendIHEResponse(ihe, jobj.toString());
        return true;
    }

    /**
     *
     * @return
     */
    private boolean registrationComplete()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj;
        JSONObject jobjreg;
        JSONObject jobjrsp = new JSONObject();
        Config.logger.trace("Registration Complete Data\n" + data);
        try
        {
            jobj = (JSONObject) jp.parse(data);
            JSONObject data1_o = (JSONObject) jobj.get("data");
            JSONObject hdr = (JSONObject) jobj.get("header");
            String dbcustcode = jobj.get("DBcustcode").toString();

            if (!data1_o.containsKey("CardNo"))
            {
                jobj.put("status", false);
                jobj.put("data", "cardno field not present");

                Utility.sendIHEResponse(ihe, jobj.toString());
                Config.logger.warn(jobj.toString());

                return false;
            }
            String cardnr = (String) data1_o.get("CardNo");

            if (!data1_o.containsKey("Mobile"))
            {
                jobj.put("status", false);
                jobj.put("data", "mobile field not present");

                Utility.sendIHEResponse(ihe, jobj.toString());
                Config.logger.warn(jobj.toString());

                return false;
            }

            String mobnr = (String) data1_o.get("Mobile");
            Card card_db = getCardInfoReg(cardnr, "ICLOSEDLOOP");
            boolean isValidCard = DBHandler.isValidCardData(cardnr, dbcustcode);

            if (dbcustcode != null)
            {
                if (isValidCard)
                {
                    if (card_db.getCardNumber() != null)
                    {
                        if (card_db.getCardStatus().trim().equals("I"))
                        {
                            if (Config.hm_data.containsKey(mobnr))
                            {
                                String hmrsp = Config.hm_data.get(mobnr);
                                jobjreg = (JSONObject) jp.parse(hmrsp);
                                JSONObject datarsp = (JSONObject) jobjreg.get("data");
                                JSONObject hdrrsp = (JSONObject) jobjreg.get("header");
                                String custcode = (String) hdrrsp.get("CustCode");
                                String fname = (String) datarsp.get("FName");
                                String lname = (String) datarsp.get("LName");
                                String email = (String) datarsp.get("Email");
                                String gender = (String) datarsp.get("Gender");
                                String dob = (String) datarsp.get("DOB");
                                String address = (String) datarsp.get("Address");
                                String country = (String) datarsp.get("Country");
                                String pincode = (String) datarsp.get("PinCode");
                                String city = (String) datarsp.get("City");
                                String state = (String) datarsp.get("State");
                                updateCustomerInfo(fname, lname, email, gender, dob, address, country, pincode, city, state, mobnr, card_db.getAntcustcode(), cardnr, card_db.getBankid());
                                jobjrsp.put("status", true);
                            } else
                            {
                                jobjrsp.put("status", false);
                                jobjrsp.put("data", "mobile mumber does not match with registration request");
                            }
                        } else
                        {
                            jobjrsp.put("status", false);
                            jobjrsp.put("data", "card number is already active");
                        }
                    } else
                    {
                        jobjrsp.put("status", false);
                        jobjrsp.put("data", "no data found for the requested cardnumber");
                    }
                } else
                {
                    jobjrsp.put("status", false);
                    jobjrsp.put("data", "card number is mapped to other custcode");
                }
            } else
            {
                jobjrsp.put("status", false);
                jobjrsp.put("data", "custcode not received");
            }

            Config.logger.trace(jobjrsp.toString());
        } catch (ParseException ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error parsing data");

            return false;
        }

        Config.logger.trace(jobjrsp.toString());

        Utility.sendIHEResponse(ihe, jobjrsp.toString());

        return true;
    }

    /**
     *
     * @return
     */
    private boolean topup()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj;

        try
        {
            jobj = (JSONObject) jp.parse(data);

            Config.logger.trace(jobj.toString());
        } catch (ParseException ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error parsing data");

            return false;
        }

        JSONObject hdr = (JSONObject) jobj.get("header");
        JSONObject data1_o = (JSONObject) jobj.get("data");

        if (!data1_o.containsKey("CardNo"))
        {
            jobj.put("status", false);
            jobj.put("data", "OTP field not present");

            Utility.sendIHEResponse(ihe, jobj.toString());
            Config.logger.warn(jobj.toString());

            return false;
        }
        long cardnr = (Long) data1_o.get("CardNo");
        String cardNum = String.valueOf(cardnr);

        if (!data1_o.containsKey("Amount"))
        {
            jobj.put("status", false);
            jobj.put("data", "amount field not present");

            Utility.sendIHEResponse(ihe, jobj.toString());
            Config.logger.warn(jobj.toString());

            return false;
        }
        long amount = (Long) data1_o.get("Amount");
        Card card_db = getCardInfo(cardNum, "ICLOSEDLOOP");
        String dbcust_code = jobj.get("DBcustcode").toString();
        String custcode = (String) hdr.get("CustCode");
        if (card_db != null)
        {
            if (dbcust_code.equals(card_db.getCustomerCode()))
            {
                if ("A".equals(card_db.getCardStatus()))
                {
                    updateBalanceAndStatement(cardNum, cardNum, card_db.getBalanceAmount() + amount,
                            card_db.getBalanceAmount(), "",
                            Iso8583JSON.TranType._62_CARD_LOAD,
                            amount);

                    jobj.put("status", true);
                    jobj.put("txndate", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                } else
                {
                    jobj.put("status", false);
                    jobj.put("data", "card is inactive");
                }
            } else
            {
                jobj.put("status", false);
                jobj.put("data", "topup cannot be performed due to custcode mismatch");
            }
        } else
        {
            jobj.put("status", false);
            jobj.put("data", "card number does not exist");
        }
        Config.logger.trace(jobj.toString());

        Utility.sendIHEResponse(ihe, jobj.toString());

        return true;
    }

    /**
     *
     */
    private boolean processTransaction()
    {
        Iso8583JSON msg = new Iso8583JSON();

        msg.parseMsg(data);
        Config.logger.debug("Message from TM\n" + msg.dumpMsg());

        String mti;

        mti = msg.getMsgType();

        switch (mti)
        {
            case Iso8583JSON.MsgTypeStr._0200_TRAN_REQ:
                processFinMsg(msg);
                break;
            case Iso8583JSON.MsgTypeStr._0420_ACQUIRER_REV_ADV:
                processRevMsg(msg);
                break;

            default:
                processRejectMsg(msg);
                break;
        }

        return true;
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean processFinMsg(Iso8583JSON msg)
    {
        String tran_type;

        tran_type = msg.getProcessingCode().getTrantype();

        switch (tran_type)
        {
            case Iso8583JSON.TranType._00_GOODS_SERVICES:
                //return processDebitMsg(msg);
                return processCardSale(msg);
 //           case Iso8583JSON.TranType._62_CARD_LOAD:
                //return processCreditMsg(msg);
 //               return processTopupMsg(msg);
//            case Iso8583JSON.TranType._31_BALANCE_INQUIRY:
//                // return processBalanceInquiry(msg);
//                return processBalanceInquiryN(msg);
            default:
                processRejectMsg(msg);
        }

        return false;
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean processRevMsg(Iso8583JSON msg)
    {
        String tran_type;

        tran_type = msg.getProcessingCode().getTrantype();

        switch (tran_type)
        {
            case Iso8583JSON.TranType._00_GOODS_SERVICES:
                return processCreditMsg(msg);
            case Iso8583JSON.TranType._62_CARD_LOAD:
                return processDebitMsg(msg);
            default:
                processRejectMsg(msg);
        }

        return false;
    }

//    private boolean processTopupMsg(Iso8583JSON msg)
//    {
//        try
//        {
//            Card card_txn = getTransactionCardInfo(msg);
//            JSONObject jobj = new JSONObject();
//            JSONObject jobjrsp = new JSONObject();
//            Object obj;
//            JSONParser jp = new JSONParser();
//
//            Config.logger.debug("Request\n" + msg.dumpMsg());
//
//            jobj.put("cardnr", msg.getField(Iso8583JSON.Bit._002_PAN));
//            jobj.put("amount", msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
//            jobj.put("dbcustcode", card_txn.getCustomerCode());
//
//            String header = sha2hash(Config.saltvalue + msg.getField(Iso8583JSON.Bit._002_PAN) + msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
//
//            Config.logger.debug("Request\n" + jobj.toJSONString());
//
//            SendMsgToMCardAPI msgtoApi = new SendMsgToMCardAPI(Config.topup, jobj.toJSONString(), header);
//            String rsp = msgtoApi.postdata();
//
//            long tran_amount = Long.parseLong(msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
//
//            Config.logger.info("Send Response\n" + rsp);
//
//            // Clear sensitive fields
//            msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
//            msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
//            msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);
//
//            msg.setRspMsgType();
//
//            if (rsp != null)
//            {
//
//                obj = jp.parse(rsp);
//
//                if (obj instanceof JSONObject)
//                {
//                    jobjrsp = (JSONObject) obj;
//                } else
//                {
//                    Utility.sendIHEResponse(ihe, "Error parsing data");
//                }
//                if (jobjrsp.containsKey("isoresponse"))
//                {
//                    String isoresponse = jobjrsp.get("isoresponse").toString();
//                    try
//                    {
//
//                        String bal_amount = "0";
//                        long balanceamount = 0;
//                        if (jobjrsp.get("balanceamount") == null || jobjrsp.get("balanceamount").equals(""))
//                        {
//                            balanceamount = 0;
//                        } else
//                        {
//                            balanceamount = Long.parseLong(jobjrsp.get("balanceamount").toString());
//                        }
//
//                        if (null != jobjrsp.get("authid").toString() && !jobjrsp.get("authid").toString().equals("000000"))
//                        {
//                            String authid = jobjrsp.get("authid").toString();
//                            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, isoresponse);
//
//                            if (msg.getField(39).equals("00"))
//                            {
//                                msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP, authid);
//                            }
//
//                            String sign;
//                            if (balanceamount > 0)
//                            {
//                                sign = "C";
//                                bal_amount = Utility.resize(Long.toString(balanceamount), 12, "0", false);
//                            } else
//                            {
//                                sign = "D";
//                                bal_amount = Utility.resize(Long.toString(-1 * balanceamount), 12, "0", false);
//                            }
//                            AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
//                                    Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
//                                    sign, bal_amount);
//
//                            msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());
//                        } else
//                        {
//                            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._05_DO_NOT_HONOUR);
//                        }
//                        Utility.sendIHEResponse(ihe, msg.toMsg());
//                    } catch (Exception ex)
//                    {
//                        Config.logger.error(ex);
//                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
//                        Utility.sendIHEResponse(ihe, msg.toMsg());
//                    }
//
//                } else
//                {
//                    msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
//                    Utility.sendIHEResponse(ihe, msg.toMsg());
//                }
//            } else
//            {
//                msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
//                Utility.sendIHEResponse(ihe, msg.toMsg());
//            }
//
//            Config.logger.debug("Response\n" + msg.dumpMsg());
//
//        } catch (ParseException ex)
//        {
//            Logger.getLogger(ProcessMsg.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NoSuchAlgorithmException ex)
//        {
//            Logger.getLogger(ProcessMsg.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return true;
//    }

    private boolean processCardSale(Iso8583JSON msg)
    {

        try
        {
            Card card_txn = getTransactionCardInfo(msg);

            Card card_db = getCardInfo(msg.getField(Iso8583JSON.Bit._002_PAN),
                    msg.getNodeInfo().getIssuer_node().trim());

            boolean validate = validateCard(msg, card_txn, card_db);

            if (validate == false)
            {
                Utility.sendIHEResponse(ihe, msg.toMsg());

                Config.logger.error("Response\n" + msg.dumpMsg());

                return false;
            }

            JSONObject jobj = new JSONObject();
            JSONObject jobjrsp = new JSONObject();
            Object obj;
            JSONParser jp = new JSONParser();

            Config.logger.debug("Request\n" + msg.dumpMsg());

            Date date = new Date();
            String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

            String mer_name = "";
            if (msg.isFieldSet(Iso8583JSON.Bit._043_CARD_ACCEPTOR_NAME_LOC))
            {
                String name_loc = msg.getField(Iso8583JSON.Bit._043_CARD_ACCEPTOR_NAME_LOC);
                mer_name = name_loc.substring(0, 23).trim();
                mer_name = mer_name.replace("MSW*", "");
            }

            jobj.put("trandate", modifiedDate);
            jobj.put("amount", msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
            jobj.put("custname", "");
            jobj.put("email", "");
            jobj.put("mobile", "");
            jobj.put("expdate", msg.getField(Iso8583JSON.Bit._014_DATE_EXPIRATION));
            jobj.put("cardno", msg.getField(Iso8583JSON.Bit._002_PAN));
            jobj.put("rrn", msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            jobj.put("jvrefno", "");
            jobj.put("userid", "");
            jobj.put("trantype", "CARDRX");
            jobj.put("merchmob", "");
            jobj.put("dbcustcode", card_txn.getCustomerCode());
            jobj.put("otptoken", msg.getField(Iso8583JSON.ExtendedBit._022_CAVV));
            jobj.put("merchantname", mer_name);

            String header = sha2hash(Config.saltvalue + msg.getField(Iso8583JSON.Bit._002_PAN));

            Config.logger.debug("Request\n" + jobj.toJSONString());
//            Config.logger.debug("Config.emisale -- > " + Config.emisale);
//            Config.logger.debug("header -- > " + header);
//            Config.logger.debug("Config.saltvalue -- > " + Config.saltvalue);
//            Config.logger.debug("msg.getField(Iso8583JSON.Bit._002_PAN) -- > " + msg.getField(Iso8583JSON.Bit._002_PAN));

            SendMsgToMCardAPI msgtoApi = new SendMsgToMCardAPI(Config.emisale, jobj.toJSONString(), header);
            String rsp = msgtoApi.postdata();
            Config.logger.info("Send Response\n" + rsp);
            msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
            msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
            msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);

            msg.setRspMsgType();

            if (rsp != null)
            {
                obj = jp.parse(rsp);

                // Clear sensitive fields
                if (obj instanceof JSONObject)
                {
                    jobjrsp = (JSONObject) obj;
                } else
                {
                    Utility.sendIHEResponse(ihe, "Error parsing data");
                }

                if (jobjrsp.containsKey("isoresponse"))
                {
                    String isoresponse = jobjrsp.get("isoresponse").toString();

                    try
                    {
                        long tran_amount = Long.parseLong(msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));

                        long balanceamount = 0;
                        String bal_amount = "0";
                        if (jobjrsp.containsKey("balanceamount"))
                        {
                            if (jobjrsp.get("balanceamount") == null || jobjrsp.get("balanceamount").equals(""))
                            {
                                balanceamount = 0;
                            } else
                            {
                                balanceamount = Long.parseLong(jobjrsp.get("balanceamount").toString());
                            }
                        } else
                        {
                            balanceamount = 0;
                        }
                        if (jobjrsp.containsKey("authid"))
                        {
                            if (null != jobjrsp.get("authid").toString() && !jobjrsp.get("authid").toString().equals("000000"))
                            {
                                String authid = jobjrsp.get("authid").toString();
                                msg.setField(Iso8583JSON.Bit._039_RSP_CODE, isoresponse);

                                if (msg.getField(39).equals("00"))
                                {
                                    msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP, authid);
                                }

                                String sign;
                                if (balanceamount > 0)
                                {
                                    sign = "C";
                                    bal_amount = Utility.resize(Long.toString(balanceamount), 12, "0", false);
                                } else
                                {
                                    sign = "D";
                                    bal_amount = Utility.resize(Long.toString(-1 * balanceamount), 12, "0", false);
                                }

                                AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
                                        Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
                                        sign, bal_amount);

                                msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());

                            } else
                            {
                                msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
                            }
                        } else
                        {
                            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, isoresponse);

                            String sign;
                            if (balanceamount > 0)
                            {
                                sign = "C";
                                bal_amount = Utility.resize(Long.toString(balanceamount), 12, "0", false);
                            } else
                            {
                                sign = "D";
                                bal_amount = Utility.resize(Long.toString(-1 * balanceamount), 12, "0", false);
                            }

                            AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
                                    Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
                                    sign, bal_amount);

                            msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());
                        }
                        Utility.sendIHEResponse(ihe, msg.toMsg());
                    } catch (Exception ex)
                    {
                        Config.logger.error(ex);
                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
                        Utility.sendIHEResponse(ihe, msg.toMsg());
                    }
                } else
                {
                    msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
                    Utility.sendIHEResponse(ihe, msg.toMsg());
                }
            } else
            {
                msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
                Utility.sendIHEResponse(ihe, msg.toMsg());
            }

            Config.logger.debug("Response\n" + msg.dumpMsg());

        } catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(ProcessMsg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex)
        {
            Logger.getLogger(ProcessMsg.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean processDebitMsg(Iso8583JSON msg)
    {
        Card card_txn = getTransactionCardInfo(msg);
        Card card_db;
        boolean validate;

        Config.logger.debug("Request\n" + msg.dumpMsg());

        card_db = getCardInfo(msg.getField(Iso8583JSON.Bit._002_PAN), msg.getNodeInfo().getIssuer_node().trim());

        validate = validateCard(msg, card_txn, card_db);

        if (validate == false)
        {
            Utility.sendIHEResponse(ihe, msg.toMsg());

            Config.logger.error("Response\n" + msg.dumpMsg());

            return false;
        }

        // Update Balances & Insert in statement
        long tran_amount = Long.parseLong(msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));

        String auth_id = updateBalanceAndStatement(card_db.getCardNumber(), card_db.getAccountNumber(),
                card_db.getBalanceAmount() - tran_amount,
                card_db.getBalanceAmount(),
                msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR),
                msg.getProcessingCode().getTrantype(), tran_amount);

        // Clear sensitive fields
        msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
        msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
        msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);

        msg.setRspMsgType();

        if (null != auth_id && !auth_id.equals("000000"))
        {
            msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP, Utility.resize(auth_id, 6, "0", false));
            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._00_SUCCESSFUL);

            String sign;
            String bal_amount;

            card_db.setBalanceAmount(card_db.getBalanceAmount() - tran_amount);

            if ((card_db.getBalanceAmount()) > 0)
            {
                sign = "D";
                bal_amount = Utility.resize(Long.toString(card_db.getBalanceAmount()), 12, "0", false);
            } else
            {
                sign = "C";
                bal_amount = Utility.resize(Long.toString(-1 * card_db.getBalanceAmount()), 12, "0", false);
            }
            AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
                    Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
                    sign, bal_amount);

            msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());
        } else
        {
            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._05_DO_NOT_HONOUR);
        }

        Utility.sendIHEResponse(ihe, msg.toMsg());

        Config.logger.debug("Response\n" + msg.dumpMsg());

        return true;
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean processCreditMsg(Iso8583JSON msg)
    {
        Card card_txn = getTransactionCardInfo(msg);
        Card card_db;
        boolean validate;

        Config.logger.debug("Request\n" + msg.dumpMsg());

        card_db = getCardInfo(msg.getField(Iso8583JSON.Bit._002_PAN), msg.getNodeInfo().getIssuer_node().trim());

        validate = validateCard(msg, card_txn, card_db);

        if (validate == false)
        {
            Utility.sendIHEResponse(ihe, msg.toMsg());

            Config.logger.error("Response\n" + msg.dumpMsg());

            return false;
        }

        // Update Balances & Insert in statement
        long tran_amount = Long.parseLong(msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));

        String auth_id = updateBalanceAndStatement(card_db.getCardNumber(), card_db.getAccountNumber(),
                card_db.getBalanceAmount() + tran_amount,
                card_db.getBalanceAmount(),
                msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR),
                msg.getProcessingCode().getTrantype(), tran_amount);

        // Clear sensitive fields
        msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
        msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
        msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);

        msg.setRspMsgType();

        if (null != auth_id && !auth_id.equals("000000"))
        {
            msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP, Utility.resize(auth_id, 6, "0", false));
            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._00_SUCCESSFUL);

            String sign;
            String bal_amount;

            card_db.setBalanceAmount(card_db.getBalanceAmount() - tran_amount);

            if ((card_db.getBalanceAmount()) > 0)
            {
                sign = "D";
                bal_amount = Utility.resize(Long.toString(card_db.getBalanceAmount()), 12, "0", false);
            } else
            {
                sign = "C";
                bal_amount = Utility.resize(Long.toString(-1 * card_db.getBalanceAmount()), 12, "0", false);
            }
            AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
                    Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
                    sign, bal_amount);

            msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());

        } else
        {
            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._05_DO_NOT_HONOUR);
        }

        Utility.sendIHEResponse(ihe, msg.toMsg());

        Config.logger.debug("Response\n" + msg.dumpMsg());

        return true;
    }

    /**
     *
     * @param msg
     */
    private void processRejectMsg(Iso8583JSON msg)
    {
        msg.setRspMsgType();
        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._30_FORMAT_ERROR);

        Utility.sendIHEResponse(ihe, msg.toMsg());

        Config.logger.warn("Response\n" + msg.dumpMsg());
    }

    /**
     *
     * @param msg
     * @return
     */
    private Card getTransactionCardInfo(Iso8583JSON msg)
    {
        Card card_txn = new Card();

        if (msg.isFieldSet(Iso8583JSON.Bit._120_TRAN_DATA_REQ))
        {
            KeyValuePair kvp = new KeyValuePair();
            HashMap<String, String> hm_kvp = kvp.formHM(msg.getField(Iso8583JSON.Bit._120_TRAN_DATA_REQ));

            if (hm_kvp.containsKey("CustCode"))
            {
                card_txn.setCustomerCode(hm_kvp.get("CustCode"));
            } else
            {
                card_txn.setCustomerCode("NA");
            }
        } else
        {
            card_txn.setCustomerCode("NA");
        }

        if (msg.isFieldSet(Iso8583JSON.Bit._002_PAN))
        {
            card_txn.setCardNumber(msg.getField(Iso8583JSON.Bit._002_PAN));
        } else
        {
            card_txn.setCardNumber("NA");
        }

        if (msg.isFieldSet(Iso8583JSON.Bit._014_DATE_EXPIRATION))
        {
            card_txn.setExpiryDate(msg.getField(Iso8583JSON.Bit._014_DATE_EXPIRATION));
        } else
        {
            card_txn.setExpiryDate("NA");
        }

        if (msg.isFieldSet(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION))
        {
            card_txn.setTxnAmount(msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
        } else
        {
            card_txn.setTxnAmount("NA");
        }

        if (msg.isFieldSet(Iso8583JSON.Bit._052_PIN_DATA))
        {
            card_txn.setPinData(msg.getField(Iso8583JSON.Bit._052_PIN_DATA));
        } else
        {
            card_txn.setPinData("NA");
        }

        card_txn.setBalanceAmount(0);

        return card_txn;
    }

    /**
     *
     * @param cardNumber
     * @param issuer_node
     * @return
     */
    private Card getCardInfo(String cardNumber, String issuer_node)
    {
        Card card = new Card();

        Connection conn;
        CallableStatement ps;
        ResultSet rs;

        boolean found = false;

        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareCall("{call cms_get_cardinfo(?,?)}");
            ps.setString(1, issuer_node);
            ps.setString(2, cardNumber);
            rs = ps.executeQuery();

            while (rs.next())
            {
                card.setCardNumber(cardNumber);
                card.setCustomerCode(rs.getString("priv_field_1").trim());
                card.setAccountNumber(rs.getString("ac_account_nr").trim());
                card.setCardStatus(rs.getString("cd_status").trim());
                card.setExpiryDate(rs.getString("cd_exp_date"));
                card.setBalanceAmount(rs.getLong("bal_avail_bal"));
                card.setOverdraft(rs.getString("overdraft"));
                card.setPinData(rs.getString("cd_offset"));
                card.setCvvkey(rs.getString("prd_cvv_key"));
                found = true;
            }

            rs.close();
            ps.close();
            conn.close();

            if (!found)
            {
                return null;
            }

            return card;
        } catch (Exception ex)
        {
            Config.logger.error(ex);
        }

        return null;
    }

    private Card getCardInfoBal(String cardNumber, String issuer_node, String dbcustcode)
    {
        Card card = new Card();

        Connection conn;
        CallableStatement ps;
        ResultSet rs;

        boolean found = false;

        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareCall("{call cms_get_cardinfo_bal(?,?,?)}");
            ps.setString(1, issuer_node);
            ps.setString(2, cardNumber);
            ps.setString(3, dbcustcode);
            rs = ps.executeQuery();

            while (rs.next())
            {
                card.setCardNumber(cardNumber);
                card.setCustomerCode(rs.getString("priv_field_1").trim());
                card.setAccountNumber(rs.getString("ac_account_nr").trim());
                card.setCardStatus(rs.getString("cd_status").trim());
                card.setExpiryDate(rs.getString("cd_exp_date"));
                card.setBalanceAmount(rs.getLong("bal_avail_bal"));

                found = true;
            }

            rs.close();
            ps.close();
            conn.close();

            if (!found)
            {
                return null;
            }

            return card;
        } catch (SQLException ex)
        {
            Config.logger.error(ex);
        }

        return null;
    }

    /**
     *
     * @param cardNumber
     * @param account_nr
     * @param balance
     * @param balance_before
     * @param rrn
     * @param tran_type
     * @param tran_amount
     * @return
     */
    private String updateBalanceAndStatement(String cardNumber, String account_nr, long balance, long balance_before,
            String rrn, String tran_type, long tran_amount)
    {
        Connection conn = null;
        CallableStatement cs;
        String auth_id = "000000";

        try
        {
            conn = Config.dbm.getConnection();

            cs = conn.prepareCall("{call cms_updatebalstmt(?,?,?,?,?,?,?)}");

            cs.setString(1, cardNumber);
            cs.setLong(2, balance);
            cs.setString(3, account_nr);
            cs.setLong(4, balance_before);
            cs.setLong(5, tran_amount);
            cs.setString(6, rrn);
            cs.setString(7, tran_type);
            cs.executeUpdate();

            cs.close();
            conn.close();

            auth_id = Config.fai.getNextAuthID();
        } catch (SQLException ex)
        {
            Config.logger.error(ex);
            Config.logger.trace(ex.toString());
        }

        return auth_id;
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean validateCard(Iso8583JSON msg, Card card_txn, Card card_db)
    {
        // Validate incoming data
        // Fetch card details from DB
        if (null == card_db)
        {
            msg.setRspMsgType();

            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._56_NO_CARD_RECORD);

            return false;
        }

        // Compare values
        if (!"A".equals(card_db.getCardStatus()))
        {
            msg.setRspMsgType();

            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._01_REFER_TO_CI);

            Config.logger.warn("Card status in DB [" + card_db.getCardStatus() + "]");

            return false;
        }

        if (card_txn.getCardNumber().equals("NA"))
        {
            msg.setRspMsgType();

            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._56_NO_CARD_RECORD);

            Config.logger.warn("No card number found");

            return false;
        }

        String currDate = Utility.getDate().substring(2, 6);
        if (Integer.parseInt(currDate) > Integer.parseInt(card_db.getExpiryDate()))
        {
            msg.setRspMsgType();

            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._54_EXPIRED_CARD);

            Config.logger.warn("Expiry Date :" + card_db.getExpiryDate() + " - " + currDate);

            return false;
        }

        try
        {
            String pem = msg.getPosEntryMode().getPanEntryMode();
            if (pem.equals("90"))
            {
                // PIN Verification
                if (card_txn.getPinData().equals("NA"))
                {
                    msg.setRspMsgType();

                    msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._05_DO_NOT_HONOUR);

                    Config.logger.warn("PIN not set in transaction");

                    return false;
                } else
                {
                    //Config.logger.debug("card_txn.getPinData()   " + card_txn.getPinData());
                    String pb_org = Sha2Pin(card_txn.getPinData());
                    //Config.logger.debug("pb_org " + pb_org);
                    //Config.logger.debug("card_db.getPinData() " + card_db.getPinData());

                    if (!pb_org.equals(card_db.getPinData()))
                    {
                        msg.setRspMsgType();

                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._55_INCORRECT_PIN);

                        Config.logger.debug("Incorrect PIN");

                        return false;
                    }
                }

                // CVV Verification
                String f_35 = msg.getField(Iso8583JSON.Bit._035_TRACK_2_DATA);
                if (null != f_35 && f_35.length() >= 27)
                {
                    String c_no = f_35.substring(0, 16);
                    String exp_Date = f_35.substring(17, 21);
                    String svc_Code = f_35.substring(21, 24);
                    String cvv = f_35.substring(24, 27);

                    String rc;

                    HSMKeyMgmt hk = new HSMKeyMgmt(Config.dbm);
                    try
                    {
                        rc = hk.verifyCVV(card_db.getCvvkey(), c_no, exp_Date, svc_Code, cvv);
                        Config.logger.info(rc);
                    } catch (SQLException ex)
                    {
                        Logger.getLogger(ProcessMsg.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }

                    if (!"00".equals(rc))
                    {
                        msg.setRspMsgType();

                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._05_DO_NOT_HONOUR);

                        Config.logger.debug("cvv missmatch");

                        return false;
                    }
                } else
                {
                    msg.setRspMsgType();

                    msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._05_DO_NOT_HONOUR);

                    Config.logger.warn("CVV is not available in Track 2 data");

                    return false;
                }
            }
        } catch (Exception ex)
        {
            Config.logger.error("Error checking PIN data");
            Config.logger.error(ex);

            msg.setRspMsgType();
            msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._96_SYSTEM_MALFUNCTION);

            return false;
        }

        return true;
    }

    /**
     *
     * @return
     */
    public static String Sha2Pin(String pb) throws Exception
    {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        String pb_org = Translate.fromBinToHex(Translate.getString(digest.digest(Translate.getData(Translate.fromHexToBin(pb)))));
        return pb_org;
    }

    public static String encrypt(String pan, String pin) throws Exception
    {
        String pb = "";
        String pin_pan = pan.substring(pan.length() - 1 - 12, pan.length() - 1);
        pin_pan = Utility.resize(pin_pan, 16, "0", false);
        System.out.println(pin_pan);
        byte[] b_pan = Translate.getData(Translate.fromHexToBin(pin_pan));
        pin = Utility.resize(Integer.toString(pin.length()), 2, "0", false) + pin;
        pin = Utility.resize(pin, 16, "F", true);
        byte[] pin_pb = Translate.getData(Translate.fromHexToBin(pin));
        byte[] pin_pb_xored = FormatData.xor(b_pan, pin_pb);
        System.out.println(FormatData.hexdump(b_pan));
        System.out.println(FormatData.hexdump(pin_pb_xored));
        pb = encryptECB(pin_pb_xored, "55eeb4af42f648deca95bdafccb17e8855eeb4af42f648deca95bdafccb17e88");
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        pb = Translate.fromBinToHex(Translate.getString(digest.digest(Translate.getData(Translate.fromHexToBin(pb)))));
        System.out.println(pb.length());
        return pb;
    }

    public static String encryptECB(byte[] inpBytes, String clearKey)
    {
        byte[] finalCipher = null;
        try
        {
            String algorithm = "DESede";
            String transformation = "DESede/ECB/PKCS5Padding";
            byte[] keyvalue = Translate.getData(Translate.fromHexToBin(clearKey));
            DESedeKeySpec keySpec = new DESedeKeySpec(keyvalue);
            SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
            Cipher encrypter = Cipher.getInstance(transformation);
            encrypter.init(Cipher.ENCRYPT_MODE, key);
            finalCipher = encrypter.doFinal(inpBytes);
        } catch (Exception ex)
        {
            return Translate.fromBinToHex(Translate.getString(finalCipher));
        }

        return Translate.fromBinToHex(Translate.getString(finalCipher));
    }

    private boolean addcards()
    {
        Connection conn;
        CallableStatement cs;
        ResultSet rs;

        JSONParser jp = new JSONParser();
        JSONObject jobj = new JSONObject();
        JSONArray jarr;
        Object obj;
        JSONObject jobj2 = new JSONObject();

        try
        {
            Config.logger.trace("Data\n" + data);
            obj = jp.parse(data);

            if (obj instanceof JSONObject)
            {
                jobj = (JSONObject) obj;
            } else if (obj instanceof JSONArray)
            {
                jarr = (JSONArray) obj;
                jobj = (JSONObject) jarr.get(0);
            } else
            {
                Utility.sendIHEResponse(ihe, "Error parsing data");
            }

            Config.logger.trace(jobj.toString());

            /*UAT*/
            int batch_nr;
            int nr_of_cards;
            String cust_code;
            String appNo;

            if (jobj.containsKey("noOfcards"))
            {
                nr_of_cards = Integer.parseInt((String) jobj.get("noOfcards"));
            } else
            {
                nr_of_cards = Integer.parseInt((String) jobj.get("NoOfcards"));
            }

            if (jobj.containsKey("custCode"))
            {
                cust_code = (String) jobj.get("custCode");
            } else
            {
                cust_code = (String) jobj.get("CustCode");
            }
            String dbaName = (String) jobj.get("dbaName");

            if (jobj.containsKey("appNo"))
            {
                appNo = (String) jobj.get("appNo");
            } else
            {
                appNo = (String) jobj.get("Applno");
            }
            String overdraft = (String) jobj.get("overdraft");
            String bintype = (String) jobj.get("bintype");

//              int nr_of_cards = Integer.parseInt((String) jobj.get("noOfcards"));
//            String cust_code = (String) jobj.get("custCode");
//            String dbaName = (String) jobj.get("dbaName");
//            String appNo = (String) jobj.get("appNo");
//            String overdraft = (String) jobj.get("overdraft");
//            
            /*Production*/
//            int nr_of_cards = Integer.parseInt((String) jobj.get("NoOfcards"));
//            String cust_code = (String) jobj.get("CustCode");
//            String dbaName = (String) jobj.get("dbaName");
//            String appNo = (String) jobj.get("Applno");
//            String overdraft = (String) jobj.get("overdraft");
            batch_nr = DBHandler.addCardBatch(1, "001", nr_of_cards, "0", overdraft, cust_code + ";" + dbaName + ";" + appNo + ";" + bintype);

            if (batch_nr == -1)
            {
                jobj2.put("status", false);
                jobj2.put("data", "Error creating batch");
            } else
            {
                jobj2.put("status", true);
                jobj2.put("batchnr", batch_nr);
            }

            Utility.sendIHEResponse(ihe, jobj2.toJSONString());

            Config.logger.info(jobj2.toString());
        } catch (Exception ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error parsing data");

            return false;
        }

        return true;
    }

    /**
     *
     * @return
     */
    private boolean balancesummary()
    {

        JSONParser jp = new JSONParser();
        JSONObject jobj = new JSONObject();
        JSONArray jarr;
        Object obj;
        JSONObject jobj2 = new JSONObject();

        try
        {
            Config.logger.trace("Data\n" + data);
            obj = jp.parse(data);

            if (obj instanceof JSONObject)
            {
                jobj = (JSONObject) obj;
            } else if (obj instanceof JSONArray)
            {
                jarr = (JSONArray) obj;
                jobj = (JSONObject) jarr.get(0);
            } else
            {
                Utility.sendIHEResponse(ihe, "Error parsing data");
            }

            Config.logger.trace(jobj.toString());
            String cust_code = jobj.get("DBcustcode").toString();

            String summary = DBHandler.getCustomerCardCountAmount(cust_code);
            String regdata = DBHandler.getRegisteredCards1(cust_code);
            String[] summary_arr = summary.split(";");
            String[] regdata_arr = regdata.split(";");

            jobj2.put("status", true);
            jobj2.put("CardCount", summary_arr[0]);
            if (Long.valueOf(summary_arr[1]) >= 0)
            {
                jobj2.put("CardAmount", Utility.formatAmount(summary_arr[1]));
            } else
            {
                jobj2.put("CardAmount", "-" + Utility.formatAmount(summary_arr[1].replaceAll("-", "")));
            }
            jobj2.put("ActiveCards", regdata_arr[0]);
            jobj2.put("InActiveCards", regdata_arr[1]);

            Utility.sendIHEResponse(ihe, jobj2.toJSONString());

            Config.logger.info(jobj2.toString());
        } catch (Exception ex)
        {
            Config.logger.error("Error processing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error processing data");

            return false;
        }

        return true;
    }

    /**
     *
     * @return
     */
    private boolean clbalancedetails()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj = new JSONObject();
        JSONArray jarr;
        Object obj;
        JSONObject jobj2 = new JSONObject();

        try
        {
            Config.logger.trace("Data\n" + data);
            obj = jp.parse(data);

            if (obj instanceof JSONObject)
            {
                jobj = (JSONObject) obj;
            } else if (obj instanceof JSONArray)
            {
                jarr = (JSONArray) obj;
                jobj = (JSONObject) jarr.get(0);
            } else
            {
                Utility.sendIHEResponse(ihe, "Error parsing data");
            }

            Config.logger.trace(jobj.toString());

            String cust_code = jobj.get("DBcustcode").toString();
            JSONArray summary_details = DBHandler.getBalanceSummaryDetails(cust_code);

            if (null == summary_details)
            {
                jobj2.put("status", false);
                jobj2.put("data", "could not fetch details");
            } else if (summary_details.contains("NOK"))
            {
                jobj2.put("status", false);
                jobj2.put("data", "no data found");
            } else
            {
                jobj2.put("status", true);
                jobj2.put("Details", summary_details);
            }
            Utility.sendIHEResponse(ihe, jobj2.toJSONString());

            Config.logger.info(jobj2.toString());
        } catch (Exception ex)
        {
            Config.logger.error("Error processing data - " + data);
            Config.logger.error(ex);

            jobj2.put("status", false);
            jobj2.put("data", "Error parsing data");

            Utility.sendIHEResponse(ihe, jobj2.toString());

            return false;
        }

        return true;
    }

//    private boolean processBalanceInquiryN(Iso8583JSON msg)
//    {
//
//        try
//        {
//            Card card_txn = getTransactionCardInfo(msg);
//            JSONObject jobjrsp = new JSONObject();
//            JSONObject jobj = new JSONObject();
//            Object obj;
//            JSONArray jarr;
//            JSONParser jp = new JSONParser();
//
//            Config.logger.debug("Request\n" + msg.dumpMsg());
//
//            jobj.put("cardnr", msg.getField(Iso8583JSON.Bit._002_PAN));
//            jobj.put("custcode", "");
//            jobj.put("dbcustcode", card_txn.getCustomerCode());
//
//            String header = sha2hash(Config.saltvalue + msg.getField(Iso8583JSON.Bit._002_PAN) + card_txn.getCustomerCode());
//
//            Config.logger.info("Send Request\n" + jobj.toJSONString());
//
//            SendMsgToMCardAPI msgtoapi = new SendMsgToMCardAPI(Config.balanceInq, jobj.toJSONString(), header);
//            String rsp = msgtoapi.postdata();
//
//            Config.logger.info("Send Response\n" + rsp);
//
//            msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
//            msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
//            msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);
//
//            msg.setRspMsgType();
//
//            if (rsp != null)
//            {
//
//                obj = jp.parse(rsp);
//
//                if (obj instanceof JSONObject)
//                {
//                    jobjrsp = (JSONObject) obj;
//                } else if (obj instanceof JSONArray)
//                {
//                    jarr = (JSONArray) obj;
//                    jobjrsp = (JSONObject) jarr.get(0);
//                } else
//                {
//                    Utility.sendIHEResponse(ihe, "Error parsing data");
//                }
//                String auth_id = Config.fai.getNextAuthID();
//                String bal_amount = "0";
//
//                long balanceamount;
//
//                if (jobjrsp.get("balanceamount") == null || jobjrsp.get("balanceamount").equals(""))
//                {
//                    balanceamount = 0;
//                } else
//                {
//                    balanceamount = Long.parseLong(jobjrsp.get("balanceamount").toString());
//                }
//
//                try
//                {
//                    String isoresponse = jobjrsp.get("isoresponse").toString();
//                    Config.logger.info("ISO Response - " + isoresponse);
//
//                    if (null != auth_id && !auth_id.equals("000000"))
//                    {
//                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, isoresponse);
//
//                        if (msg.getField(39).equals("00"))
//                        {
//                            msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP, Utility.resize(auth_id, 6, "0", false));
//                        }
//
//                        String sign;
//                        if (balanceamount >= 0)
//                        {
//                            sign = "C";
//                            bal_amount = Utility.resize(Long.toString(balanceamount), 12, "0", false);
//                        } else
//                        {
//                            sign = "D";
//                            bal_amount = Utility.resize(Long.toString(-1 * balanceamount), 12, "0", false);
//                        }
//
//                        AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
//                                Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
//                                sign, bal_amount);
//                        msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());
//                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, isoresponse);
//
//                    } else
//                    {
//                        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583.RspCode._05_DO_NOT_HONOUR);
//                    }
//
//                } catch (Exception ex)
//                {
//                    Config.logger.info(ex);
//                }
//                Utility.sendIHEResponse(ihe, msg.toMsg());
//            }
//            Config.logger.debug("Response\n" + msg.dumpMsg());
//
//        } catch (NoSuchAlgorithmException ex)
//        {
//            Logger.getLogger(ProcessMsg.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex)
//        {
//            Logger.getLogger(ProcessMsg.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//        return true;
//    }

    public String sha2hash(String verify) throws NoSuchAlgorithmException
    {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] passBytes = verify.getBytes();
        byte[] passHash = sha256.digest(passBytes);
        return Translate.fromBinToHex(Translate.getString(passHash));
    }

    /**
     * @deprecated @param msg
     * @return
     */
    private boolean processBalanceInquiry(Iso8583JSON msg)
    {
        Card card_txn = getTransactionCardInfo(msg);
        Card card_db;
        boolean validate;

        Config.logger.debug("Request\n" + msg.dumpMsg());

        card_db = getCardInfo(msg.getField(Iso8583JSON.Bit._002_PAN), msg.getNodeInfo().getIssuer_node().trim());

        validate = validateCard(msg, card_txn, card_db);

        if (validate == false)
        {
            Utility.sendIHEResponse(ihe, msg.toMsg());

            Config.logger.error("Response\n" + msg.dumpMsg());

            return false;
        }

        // Clear sensitive fields
        msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
        msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
        msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);

        msg.setRspMsgType();

        msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP, Utility.resize("MSWIPE", 6, "0", false));
        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._00_SUCCESSFUL);

        String sign;
        String bal_amount;

        if ((card_db.getBalanceAmount()) > 0)
        {
            sign = "D";
            bal_amount = Utility.resize(Long.toString(card_db.getBalanceAmount()), 12, "0", false);
        } else
        {
            sign = "C";
            bal_amount = Utility.resize(Long.toString(-1 * card_db.getBalanceAmount()), 12, "0", false);
        }
        AdditionalAmount aa = new AdditionalAmount(msg.getProcessingCode().getFromaccount(),
                Iso8583JSON.AmountType._02_AVAILABLE_BALANCE, msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN),
                sign, bal_amount);

        msg.setField(Iso8583JSON.Bit._054_ADDITIONAL_AMOUNTS, aa.getAdditionalAmountStr());

        Utility.sendIHEResponse(ihe, msg.toMsg());

        Config.logger.debug("Response\n" + msg.dumpMsg());

        return true;
    }

    /**
     *
     * @param mobileNr
     * @param msg
     */
    private void sendSMS(String mobileNr, String msg)
    {
        String msg_remote;
        StringBuilder sb = new StringBuilder();
        msg_remote = msg + " is the Onetime Password (OTP) "
                + "for new card registration. "
                + "It is valid for 15 minutes only.";

        mobileNr = "91" + mobileNr;

        msg_remote = "MobileNr=" + mobileNr + "&Msg=" + msg_remote;
        Config.logger.info("Msg\n" + msg_remote);

        String rsp = Utility.sendAndRxHTTPMsg(Config.smsGateway, 10000, msg_remote);
        Config.logger.info("Send SMS\n" + rsp);
    }

    /**
     *
     * @return
     */
    private boolean verifyOTP()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj;
        JSONObject jobj2 = new JSONObject();
        ConcurrentHashMap<String, Long> hm_opt1 = new ConcurrentHashMap<>();
        String mobilenr;
        long otp;
        long s_otp;

        try
        {
            Config.logger.trace("Data\n" + data);
            jobj = (JSONObject) jp.parse(data);

            JSONObject data1_o = (JSONObject) jobj.get("data");
            if (!data1_o.containsKey("OTP"))
            {
                jobj2.put("status", false);
                jobj2.put("data", "OTP field not present");

                Utility.sendIHEResponse(ihe, jobj2.toString());
                Config.logger.warn(jobj2.toString());

                return false;
            }
            otp = (Long) data1_o.get("OTP");

            if (!data1_o.containsKey("Mobile"))
            {
                jobj2.put("status", false);
                jobj2.put("data", "Mobile field not present");

                Utility.sendIHEResponse(ihe, jobj2.toString());
                Config.logger.warn(jobj2.toString());

                return false;
            }
            mobilenr = (String) data1_o.get("Mobile");

            if (Config.hm_otp.containsKey(mobilenr))
            {

                s_otp = Config.hm_otp.get(mobilenr);

                if (s_otp == otp)
                {
                    jobj2.put("status", true);
                    jobj2.put("data", "Valid OTP");
                    Config.hm_otp.remove(mobilenr);

                } else
                {
                    jobj2.put("status", false);
                    jobj2.put("data", "Invalid OTP");

                    Config.logger.trace("SOTP = " + s_otp + " / OTP = " + otp);
                }
            } else
            {

                jobj2.put("status", false);
                jobj2.put("data", "mobile number not present");

            }
            Config.logger.trace(jobj2.toString());
        } catch (Exception ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            jobj2.put("status", false);
            jobj2.put("data", "Error parsing data");

            Utility.sendIHEResponse(ihe, jobj2.toString());

            return false;
        }

        Config.logger.trace(jobj2.toString());

        Utility.sendIHEResponse(ihe, jobj2.toString());

        return true;

    }

    private boolean updateBulkcards()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj = new JSONObject();
        JSONObject jobj2 = new JSONObject();
        JSONArray jarr = null;
        String applicationNr;
        String custcode;
        Object obj;
        int updcount = 0;
        try
        {
            Config.logger.trace("Data\n" + data);
            obj = jp.parse(data);

            if (obj instanceof JSONArray)
            {
                jarr = (JSONArray) obj;
            } else
            {
                Utility.sendIHEResponse(ihe, "Error parsing data");
            }

            Config.logger.info("Array Size - " + jarr.size());
            for (int i = 0; i < jarr.size(); i++)
            {
                jobj = (JSONObject) jarr.get(i);

                Config.logger.trace(jobj.toString());

                if (!jobj.containsKey("appl_No"))
                {
                    jobj2.put("status", false);
                    jobj2.put("data", "appl_No field not present");
                    jobj2.put("responsecode", "201");

                    Utility.sendIHEResponse(ihe, jobj2.toString());
                    Config.logger.warn(jobj2.toString());

                    return false;
                }
                applicationNr = jobj.get("appl_No").toString();

                if (!jobj.containsKey("cust_Code"))
                {
                    jobj2.put("status", false);
                    jobj2.put("data", "cust_Code field not present");
                    jobj2.put("responsecode", "201");

                    Utility.sendIHEResponse(ihe, jobj2.toString());
                    Config.logger.warn(jobj2.toString());

                    return false;
                }

                custcode = jobj.get("cust_Code").toString();

                updcount = DBHandler.updateAppNrtoCards(applicationNr, custcode);

            }

            jobj2.put("status", true);
            jobj2.put("data", "data has been updated");
            jobj2.put("responsecode", "201");
            Utility.sendIHEResponse(ihe, jobj2.toJSONString());

            Config.logger.info(jobj2.toString());

        } catch (Exception ex)
        {

            //System.out.println(ex.toString());
            Config.logger.error("Error processing data - " + data);
            Config.logger.error(ex);

            jobj2.put("status", false);
            jobj2.put("data", "Error parsing data");

            Utility.sendIHEResponse(ihe, jobj2.toString());

            return false;
        }

        return true;
    }

    private void updateCustomerInfo(String fname, String lname, String email, String gender, String dob, String address, String country, String pincode, String city, String state, String mobnr, String custcode, String cardnr, int bankid)
    {
        try
        {
            Connection conn = null;
            CallableStatement cs = null;
            PreparedStatement ps = null;
            conn = Config.dbm.getConnection();
            ResultSet rs;

            String query = "{call usp_add_cms_customer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            cs = conn.prepareCall(query);
            cs.setInt(1, bankid);// bankid
            cs.setString(2, custcode);// cust id
            cs.setString(3, " ");//salutation
            cs.setString(4, fname);//first name
            cs.setString(5, " ");//middle name
            cs.setString(6, lname);//last name
            cs.setString(7, fname);//emboss name
            cs.setString(8, " ");//emboss name2
            cs.setString(9, " ");//embossname3
            cs.setString(10, address);//addr1
            cs.setString(11, "Address2");
            cs.setString(12, "Address3");
            cs.setString(13, city);//city
            cs.setString(14, state);
            cs.setString(15, "IN");
            cs.setString(16, "400011");
            cs.setString(17, address);
            cs.setString(18, "Address2");
            cs.setString(19, "Address3");
            cs.setString(20, city);//city
            cs.setString(21, city);
            cs.setString(22, "IN");
            cs.setString(23, "400011");
            cs.setString(24, mobnr);
            cs.setString(25, "8888888888");
            cs.setString(26, mobnr);
            cs.setString(27, email);
            cs.setString(28, dob);
            cs.setString(29, " ");

            cs.execute();
            cs.close();

            Statement stmt = null;
            stmt = conn.createStatement();
            String updQuery = "update cms_cards set cd_status='A',cd_reg_datetime=getdate() where cd_card='" + cardnr + "';";
            int count = stmt.executeUpdate(updQuery);
            stmt.close();
            conn.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(ProcessMsg.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Card getCardInfoReg(String cardNumber, String issuer_node)
    {
        Card card = new Card();

        Connection conn;
        CallableStatement ps;
        ResultSet rs;

        boolean found = false;

        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareCall("{call cms_get_cardinfo_reg(?,?)}");
            ps.setString(1, issuer_node);
            ps.setString(2, cardNumber);
            rs = ps.executeQuery();

            while (rs.next())
            {
                card.setCardNumber(cardNumber);
                card.setBankid(rs.getInt("cd_bank_id"));
                card.setAntcustcode(rs.getString("cd_cust_id").trim());
                card.setCustomerCode(rs.getString("priv_field_1").trim());
                card.setAccountNumber(rs.getString("ac_account_nr").trim());
                card.setCardStatus(rs.getString("cd_status").trim());
                card.setExpiryDate(rs.getString("cd_exp_date"));
                card.setBalanceAmount(rs.getLong("bal_avail_bal"));

                found = true;
            }

            rs.close();
            ps.close();
            conn.close();

            if (!found)
            {
                return null;
            }

            return card;
        } catch (SQLException ex)
        {
            Config.logger.error(ex);
        }

        return null;
    }

    private boolean registeredinfo()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj = new JSONObject();
        JSONArray jarr;
        Object obj;
        JSONObject jobj2 = new JSONObject();

        try
        {
            Config.logger.trace("Data\n" + data);
            obj = jp.parse(data);

            if (obj instanceof JSONObject)
            {
                jobj = (JSONObject) obj;
            } else if (obj instanceof JSONArray)
            {
                jarr = (JSONArray) obj;
                jobj = (JSONObject) jarr.get(0);
            } else
            {
                Utility.sendIHEResponse(ihe, "Error parsing data");
            }

            Config.logger.trace(jobj.toString());

            String cust_code = jobj.get("DBcustcode").toString();
            JSONArray summary_details = DBHandler.getRegisteredCards(cust_code);

            if (null == summary_details)
            {
                jobj2.put("status", false);
                jobj2.put("data", "could not fetch details");
            } else if (summary_details.contains("NOK"))
            {
                jobj2.put("status", false);
                jobj2.put("data", "no data found");
            } else
            {
                jobj2.put("status", true);
                jobj2.put("Details", summary_details);
            }
            Utility.sendIHEResponse(ihe, jobj2.toJSONString());

            Config.logger.info(jobj2.toString());
        } catch (Exception ex)
        {
            Config.logger.error("Error processing data - " + data);
            Config.logger.error(ex);

            jobj2.put("status", false);
            jobj2.put("data", "Error parsing data");

            Utility.sendIHEResponse(ihe, jobj2.toString());

            return false;
        }

        return true;
    }

    private boolean resendOTP()
    {
        JSONParser jp = new JSONParser();
        JSONObject jobj;
        JSONObject jobj2 = new JSONObject();
        String mobilenr;
        long otp;
        long s_otp;

        try
        {
            Config.logger.trace("Data\n" + data);
            jobj = (JSONObject) jp.parse(data);

            JSONObject data1_o = (JSONObject) jobj.get("data");
            if (!data1_o.containsKey("OTP"))
            {
                jobj2.put("status", false);
                jobj2.put("data", "OTP field not present");

                Utility.sendIHEResponse(ihe, jobj2.toString());
                Config.logger.warn(jobj2.toString());

                return false;
            }
            otp = (Long) data1_o.get("OTP");

            if (!data1_o.containsKey("Mobile"))
            {
                jobj2.put("status", false);
                jobj2.put("data", "Mobile field not present");

                Utility.sendIHEResponse(ihe, jobj2.toString());
                Config.logger.warn(jobj2.toString());

                return false;
            }
            mobilenr = (String) data1_o.get("Mobile");

            if (Config.hm_otp.containsKey(mobilenr))
            {

                s_otp = Config.hm_otp.get(mobilenr);

                Thread th = new Thread(()
                        -> 
                        {
                            sendSMS((String) data1_o.get("Mobile"), Long.toString(otp));
                });
                th.start();

//                if (s_otp == otp) {
//                    jobj2.put("status", true);
//                    jobj2.put("data", "Valid OTP");
//                    Config.hm_otp.remove(mobilenr);
//
//                } else {
//                    jobj2.put("status", false);
//                    jobj2.put("data", "Invalid OTP");
//
//                    Config.logger.trace("SOTP = " + s_otp + " / OTP = " + otp);
//                }
            } else
            {

                jobj2.put("status", false);
                jobj2.put("data", "mobile number not present");

            }
            Config.logger.trace(jobj2.toString());
        } catch (Exception ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            jobj2.put("status", false);
            jobj2.put("data", "Error parsing data");

            Utility.sendIHEResponse(ihe, jobj2.toString());

            return false;
        }

        Config.logger.trace(jobj2.toString());

        Utility.sendIHEResponse(ihe, jobj2.toString());

        return true;

    }

    private boolean addcardn()
    {
        Connection conn;
        CallableStatement cs;
        ResultSet rs;

        JSONParser jp = new JSONParser();
        JSONObject jobj = new JSONObject();
        JSONObject jobj1 = new JSONObject();
        JSONArray jarr = null;
        JSONArray jarrRsp = new JSONArray();
        Object obj;
        JSONObject jobj2 = new JSONObject();
        try
        {
            Config.logger.trace("Data\n" + data);
            obj = jp.parse(data);

            if (obj instanceof JSONObject)
            {
                jobj = (JSONObject) obj;
            } else if (obj instanceof JSONArray)
            {
                jarr = (JSONArray) obj;
                //   jobj = (JSONObject) jarr.get(0);
            } else
            {
                Utility.sendIHEResponse(ihe, "Error parsing data");
            }

            Config.logger.trace(jobj.toString());

            int nr_of_cards;
            String cust_code;
            String appNo;
            String cardtype = "MCARD";
            String printlogo = "N";

            Config.logger.trace(jarr.size());
            for (int i = 0; i < jarr.size(); i++)
            {
                jobj = (JSONObject) jarr.get(i);

                Config.logger.trace(jobj.toString());

                if (jobj.containsKey("printlogo"))
                {
                    printlogo = (String) jobj.get("printlogo");
                    if (printlogo == null || printlogo.equals(""))
                    {
                        printlogo = "N";
                    }
                } else
                {
                    printlogo = (String) jobj.get("PrintLogo");
                    if (printlogo == null || printlogo.equals(""))
                    {
                        printlogo = "N";
                    }
                }

                if (jobj.containsKey("CardType"))
                {
                    cardtype = (String) jobj.get("CardType");
                } else
                {
                    cardtype = (String) jobj.get("cardType");
                }

                if (jobj.containsKey("noOfcards"))
                {
                    nr_of_cards = Integer.parseInt((String) jobj.get("noOfcards"));
                } else
                {
                    nr_of_cards = Integer.parseInt((String) jobj.get("NoOfcards"));
                }

                if (jobj.containsKey("custCode"))
                {
                    cust_code = (String) jobj.get("custCode");
                } else
                {
                    cust_code = (String) jobj.get("CustCode");
                }

                if (jobj.containsKey("appNo"))
                {
                    appNo = (String) jobj.get("appNo");
                } else
                {
                    appNo = (String) jobj.get("Applno");
                }

                String dbaName = (String) jobj.get("dbaName");
                String overdraft = (String) jobj.get("overdraft");
                String cardtheme = (String) jobj.get("cardtheme");
                String bintype = null;
                if (!jobj.containsKey("bintype"))
                {
                    bintype = (String) jobj.get("bintype");
                    jobj1 = DBHandler.addNCardBatch(1, "001", nr_of_cards, "4", overdraft, cust_code + ";" + dbaName + ";" + appNo + ";" + bintype + ";" + cardtheme + ";" + cardtype, bintype, cust_code, printlogo, "BinType not received");
                } else
                {
                    bintype = (String) jobj.get("bintype");
                    if (cust_code == null || cust_code.equals(""))
                    {
                        jobj1 = DBHandler.addNCardBatch(1, "001", nr_of_cards, "3", overdraft, cust_code + ";" + dbaName + ";" + appNo + ";" + bintype + ";" + cardtheme + ";" + cardtype, bintype, cust_code, printlogo, "Cust Code Pending");
                    } else
                    {
                        jobj1 = DBHandler.addNCardBatch(1, "001", nr_of_cards, "0", overdraft, cust_code + ";" + dbaName + ";" + appNo + ";" + bintype + ";" + cardtheme + ";" + cardtype, bintype, cust_code, printlogo, "Pending");
                    }
                }
                jarrRsp.add(jobj1);

                jobj2.put("details", jarrRsp);

            }
            Utility.sendIHEResponse(ihe, jarrRsp.toString());

            Config.logger.info(jarrRsp);

        } catch (Exception ex)
        {
            Config.logger.error("Error parsing data - " + data);
            Config.logger.error(ex);

            Utility.sendIHEResponse(ihe, "Error parsing data");

            return false;
        }

        return true;
    }

}
