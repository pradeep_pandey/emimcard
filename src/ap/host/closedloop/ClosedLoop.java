package ap.host.closedloop;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import ap.sdk.comms.XHttpServer;
import ap.sdk.processor.BaseModule;
import ap.sdk.util.MQueue;
import ap.sdk.util.ProcessStatusUpdate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Abhishek1.M
 */
public class ClosedLoop extends BaseModule
{

    int listenPort;
    int backlog;
    String context;
    String listenip;
    long idletimeout;
    private ExecutorService pool;
    int threadPool;
    XHttpServer hsm_server;

    /**
     *
     */
    public ClosedLoop()
    {
        super(new MQueue());

        Config.initializeConfigurations();
        pool = Executors.newCachedThreadPool();

        Config.logger.info("Startup - Done.");
    }

    /**
     *
     */
    public void startprocess()
    {
        String sql = "select listenip, listenport, backlog, idletimeout from onl_process where pname=?";
        Connection conn;
        PreparedStatement ps;
        ResultSet rs;
        boolean found;

        try
        {
            conn = Config.dbm.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, Config._MODULE_NAME);

            found = false;

            rs = ps.executeQuery();

            while (rs.next())
            {
                listenip = rs.getString(1);
                listenPort = rs.getInt(2);
                backlog = rs.getInt(3);
                idletimeout = rs.getInt(4);

                found = true;
            }

            rs.close();
            ps.close();
            conn.close();

            if (!found)
            {
                Config.logger.fatal("Fetch Process Name - Process " + Config._MODULE_NAME + " not found.");
                shutdown();
            }

            pool.execute(new ProcessStatusUpdate(Config._MODULE_NAME, Config.dbm));

            hsm_server = new XHttpServer(Config._MODULE_NAME, listenip,
                    listenPort, getQueue(), backlog, idletimeout);

            start();
            
            Config.logger.info("ClosedLoop Service started");
        } catch (IOException | SQLException ex)
        {
            Config.logger.error(ex);
            System.exit(1);
        }
    }

    /**
     *
     */
    public void shutdown()
    {
        pool.shutdown();

        System.exit(0);
    }

    /**
     *
     * @param event
     */
    @Override
    public void processEvent(Object event)
    {
        if (event instanceof XHttpServer.DataEvent)
        {
            XHttpServer.DataEvent de = (XHttpServer.DataEvent) event;
            pool.execute(new ProcessMsg(de.ihe, de.data));
        }
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        ClosedLoop cl = new ClosedLoop();
        cl.startprocess();
    }
}
