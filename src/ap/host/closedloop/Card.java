package ap.host.closedloop;

/**
 *
 * @author abhi
 */
public class Card
{

    private String cardNumber;
    private String customerCode;
    private String expiryDate;
    private long balanceAmount;
    private String cardStatus;
    private String accountNumber;
    private int bankid;
    private double balAmount;
    private String antcustcode;
    private String txnAmount;
    private String overdraft;
    private String cardtheme;
    private String printlogo;
    private String pinData;
    private String cvvkey;

    public void setCvvkey(String cvvkey)
    {
        this.cvvkey = cvvkey;
    }

    public String getCvvkey()
    {
        return cvvkey;
    }

    public String getPinData()
    {
        return pinData;
    }

    public void setPinData(String pinData)
    {
        this.pinData = pinData;
    }

    public String getCardtheme()
    {
        return cardtheme;
    }

    public void setCardtheme(String cardtheme)
    {
        this.cardtheme = cardtheme;
    }

    public String getPrintlogo()
    {
        return printlogo;
    }

    public void setPrintlogo(String printlogo)
    {
        this.printlogo = printlogo;
    }

    public String getOverdraft()
    {
        return overdraft;
    }

    public void setOverdraft(String overdraft)
    {
        this.overdraft = overdraft;
    }

    public String getTxnAmount()
    {
        return txnAmount;
    }

    public void setTxnAmount(String txnAmount)
    {
        this.txnAmount = txnAmount;
    }

    public String getAntcustcode()
    {
        return antcustcode;
    }

    public void setAntcustcode(String antcustcode)
    {
        this.antcustcode = antcustcode;
    }

    public double getBalAmount()
    {
        return balAmount;
    }

    public void setBalAmount(double balAmount)
    {
        this.balAmount = balAmount;
    }

    public int getBankid()
    {
        return bankid;
    }

    public void setBankid(int bankid)
    {
        this.bankid = bankid;
    }

    public String getCardNumber()
    {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber)
    {
        this.cardNumber = cardNumber;
    }

    public String getCustomerCode()
    {
        return customerCode;
    }

    public void setCustomerCode(String customerCode)
    {
        this.customerCode = customerCode;
    }

    public String getExpiryDate()
    {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public long getBalanceAmount()
    {
        return balanceAmount;
    }

    public void setBalanceAmount(long balanceAmount)
    {
        this.balanceAmount = balanceAmount;
    }

    public String getCardStatus()
    {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus)
    {
        this.cardStatus = cardStatus;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

}
