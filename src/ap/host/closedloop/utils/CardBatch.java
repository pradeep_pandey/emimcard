package ap.host.closedloop.utils;

public class CardBatch 
{
    long cb_batch_nr;
    int cb_count;
    int bank_id;
    String cb_disc_data;
    String overdraft;
    String printlogo;
    String custcode;
    String remarks;
    

    public CardBatch(long cb_batch_nr, int cb_count, int bank_id, String cb_disc_data, String overdraft, String printlogo,String custcode, String remarks) {
        this.cb_batch_nr = cb_batch_nr;
        this.cb_count = cb_count;
        this.bank_id = bank_id;
        this.cb_disc_data = cb_disc_data;
        this.overdraft = overdraft;
        this.printlogo = printlogo;
        this.custcode = custcode;
        this.remarks = remarks;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }    

    public long getCb_batch_nr() {
        return cb_batch_nr;
    }

    public void setCb_batch_nr(long cb_batch_nr) {
        this.cb_batch_nr = cb_batch_nr;
    }

    public int getCb_count() {
        return cb_count;
    }

    public void setCb_count(int cb_count) {
        this.cb_count = cb_count;
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public String getCb_disc_data() {
        return cb_disc_data;
    }

    public void setCb_disc_data(String cb_disc_data) {
        this.cb_disc_data = cb_disc_data;
    }

    public String getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(String overdraft) {
        this.overdraft = overdraft;
    }

    public String getPrintlogo() {
        return printlogo;
    }

    public void setPrintlogo(String printlogo) {
        this.printlogo = printlogo;
    }
    
    
}
