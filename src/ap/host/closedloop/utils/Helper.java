/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.host.closedloop.utils;

import ap.host.closedloop.db.DBM;
import ap.sdk.jdbc.DBManager;
import ap.sdk.util.ELogger;
import ap.sdk.util.FetchProperty;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author bhagyashree
 */
public class Helper {

    public static Properties prop = new Properties();
    public static DBManager dbm;
    //public static DBM dbm;
    public static String cardbatchkey;
    public static ELogger cdlogger;
    public static String erpcardstatusurl;
    public static String cdlogpath;
    public static String mcardfilepath;
    public static String emierpcardposturl;
    public static String mcardkey;
    public static String emicardposturl;

    public static void loadConfigProperties(String path) {
        try {
            FileInputStream fis_sql = null;
            File sql_file = new File(path);
            fis_sql = new FileInputStream(sql_file);
            prop.load(fis_sql);
        } catch (IOException ex) {
            System.out.println("Unable to load config file - " + ex.getMessage());
        }
    }

    public static void loadDBConfigurations() {
           dbm = new DBManager();
    }

    public static void loadConfigprops() {
        cardbatchkey = FetchProperty.getValue("cardbatchkey");
        if (null == cardbatchkey) {
            cardbatchkey = "NA";
        }
        cdlogpath = FetchProperty.getValue("EMICardDataPost");
        if (null == cdlogpath) {
            cdlogpath = "NA";
        }

        erpcardstatusurl = FetchProperty.getValue("MCardErpCardStatusUrl");
        if (null == erpcardstatusurl) {
            erpcardstatusurl = "NA";
        }
        
        mcardfilepath = FetchProperty.getValue("mcardfilepath");//path where track file generated
        emierpcardposturl = FetchProperty.getValue("emierpcardpost");//erp post after card generation
        mcardkey = FetchProperty.getValue("cardbatchkey");//used as salt during pan encription
        emicardposturl = FetchProperty.getValue("emicardpost");//emi post to rahul
    }

    public static void loadLogger() {
        cdlogger = new ELogger(cdlogpath, 1000, 10000);
    }

}
