package ap.host.closedloop.utils;

import ap.host.closedloop.db.DBHandler;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author bhagyashree
 */
public class EMICardPost
{

    String jsondata;
    int batchcount;
    String cardslist;
    Object obj;
    JSONObject jobj = new JSONObject();

    public static void main(String[] args)
    {
        if (args.length != 2)
        {
            System.out.println("Invalid Arguments. Exiting");
            System.exit(0);
        }
        
        Helper.loadDBConfigurations();
        Helper.loadConfigprops();
        Helper.loadLogger();

        //args = new String[]{"/home/bhagyashree/WorkSpace/Developments/Switch/Core/CL/Version1.0/CloseLoopUtil/src/ap/host/closedloop/utils/mcardgen.properties"};
        EMICardPost mpost = new EMICardPost();
        int count = Integer.parseInt(args[1]);
        String type = args[0].trim();
        mpost.processPosting(count, type);
    }

    public boolean processPosting(int count, String type)
    {
        String emiJson = new DBHandler().getEMIData(count, type);
        if (emiJson == null)
        {
            Helper.cdlogger.log("ERPCardPost", "processPosting", "INFO", "No Data found - ", "No Data found");
            return false;

        }
        String[] args1 = emiJson.split(";");
        jsondata = args1[0];
        batchcount = Integer.parseInt(args1[1]);
        cardslist = args1[2];
        Helper.cdlogger.log("ERPCardPost", "processPosting", "INFO", "Json Data to be Posted -\n", jsondata);

        if (batchcount == 0)
        {
            Helper.cdlogger.log("ERPCardPost", "processPosting", "INFO", "No Data found - ", "No Data found");
            return false;
        }
        //System.out.println("Request jsondata --> " + jsondata);
        String post_response = sendmsgtoremote(jsondata, batchcount);
        Helper.cdlogger.log("ERPCardPost", "processPosting", "INFO", "Post data response -\n", post_response);
        System.out.println(post_response);

        if (!post_response.equalsIgnoreCase("NOK"))
        {
            try
            {
                JSONParser jp = new JSONParser();
                obj = jp.parse(post_response);

                if (obj instanceof JSONObject)
                {
                    jobj = (JSONObject) obj;
                }

                String status = jobj.get("status").toString();

                if (status.equalsIgnoreCase("Success"))
                {
                    new DBHandler().updateemicardsposted(cardslist);
                }
            } catch (ParseException ex)
            {
                Logger.getLogger(EMICardPost.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return true;
    }

    public String sha2hash(String verify) throws NoSuchAlgorithmException
    {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] passBytes = verify.getBytes();
        byte[] passHash = sha256.digest(passBytes);
        return Translate.fromBinToHex(Translate.getString(passHash));
    }

    
    private String sendmsgtoremote(String jsondata, int batchcount)
    {
        String jsonData = "NOK";
        try
        {

            URL url;
            URLConnection uconn;
            int timeout = 50000;

            DataOutputStream dos;
            DataInputStream dis;

            String header = sha2hash(Helper.mcardkey + Utility.resize(String.valueOf(batchcount), 12, "0", false));

            url = new URL(Helper.emicardposturl);
            uconn = url.openConnection();
            uconn.setRequestProperty("key", header);
            uconn.setRequestProperty("Content-Type", "application/json");
            uconn.setRequestProperty("charset", "utf-8");
            uconn.setReadTimeout(timeout);
            uconn.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(uconn.getOutputStream());
            out.write(jsondata);
            out.close();

            dis = new DataInputStream(uconn.getInputStream());

            StringBuilder str = new StringBuilder();
            int x;
            while ((x = dis.read()) != -1)
            {
                str.append((char) x);
            }

            jsonData = str.toString();
            dis.close();
        } catch (MalformedURLException ex)
        {
            System.out.println("MalformedURLException - " + ex.getMessage());
        } catch (IOException ex)
        {
            System.out.println("IOException - " + ex.getMessage());
        } catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(EMICardPost.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonData;

    }
}
