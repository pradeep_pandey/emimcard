package ap.host.closedloop.utils;

import ap.host.closedloop.db.DBHandler;
import static ap.host.closedloop.utils.Helper.cdlogger;
import ap.sdk.crypto.HSMKeyMgmt;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import com.mswipe.model.CIFCG;
import com.mswipe.model.ProdDetails;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author abhi
 */
public class EMIGenerateCards
{

    // DBManager dbm;
    List<CIFCG> cifreqlist = null;
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd_HHmmss");
    Date currdate = new Date();
    Object obj;
    JSONObject jobj = new JSONObject();
    String cardslist;

    public void process()
    {

        cifreqlist = new DBHandler().getCIFRequestData();
        FileWriter fw = null;
        File file = new File(Helper.mcardfilepath + "EMI" + "_" + sdf1.format(currdate) + ".txt");
        try
        {
            try
            {
                fw = new FileWriter(file, true);
            } catch (IOException ex)
            {
                Logger.getLogger(EMIGenerateCards.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cifreqlist != null)
            {
                for (CIFCG cifcg : cifreqlist)
                {
                    try
                    {
                        cdlogger.log("EMICARDGeneration", "process", "INFO", "Card Generation started for batch nr -  - ", "" + cifcg.getRowid());

                        Date dt = getExpiry(60);
                        DateFormat sdf = new SimpleDateFormat("yyMM");
                        String fromdate = sdf.format(new Date());
                        String expirydate = sdf.format(dt);
                        //cdlogger.log("EMICARDGeneration", "process", "INFO", "BIN - ", cifcg.getBin());
                        String maxCard = new DBHandler().getMaxCard(cifcg.getBin());

                        ProdDetails prodetails = new DBHandler().getProductDetails(cifcg.getBin());
                        if (null == maxCard)
                        {
                            maxCard = Utility.resize(cifcg.getBin().trim(), 16, "0", true);
                        }
                        //cdlogger.log("EMICARDGeneration", "process", "INFO", "maxCard - ", maxCard);
                        String ncard = maxCard;
                        ncard = getNextCard(ncard);
                        String enc_pan = sha2hash(Helper.mcardkey + ncard);
                        //System.out.println("enc_pan -- > " + enc_pan);
                        new DBHandler().addmcards(ncard, expirydate, "", "", "", "", enc_pan, cifcg);

                        StringBuilder sbd = new StringBuilder();
                        String new_dbaname = cifcg.getName();
                        HSMKeyMgmt hk = new HSMKeyMgmt(Helper.dbm);
                        String cvv = hk.getCVV(prodetails.getPrd_cvv_key(), ncard, expirydate, prodetails.getPrd_srv_code());

                        if (new_dbaname.length() > 26)
                        {
                            new_dbaname = new_dbaname.substring(0, 26).toUpperCase();
                        } else
                        {
                            new_dbaname = new_dbaname.trim().toUpperCase();
                        }

                        String result21 = sbd
                                .append(ncard)
                                .append(",")
                                .append("%B")
                                .append(ncard)
                                .append("^")
                                .append(new_dbaname)
                                .append("/^")
                                .append(expirydate)
                                .append(prodetails.getPrd_srv_code())
                                .append(cvv)
                                .append("?")
                                .append(",")
                                .append(";")
                                .append(ncard)
                                .append("=")
                                .append(expirydate)
                                .append(prodetails.getPrd_srv_code())
                                .append(cvv)
                                .append("?,")
                                .append(new_dbaname)
                                .append(",")
                                .append(fromdate).toString();

                        fw.write(result21 + "\n");
                        //Helper.mcard_gen_logger.debug(result21);
                        new DBHandler().updateemibatchnr(cifcg.getRowid(), ncard);
                        cdlogger.log("EMICARDGeneration", "process", "INFO", "mCard Generated for batch nr - ", "" + cifcg.getRowid());

                        JSONObject array = new DBHandler().getEMIERPData(ncard);
                        cdlogger.log("EMICARDGeneration", "process", "INFO", "JSONObject - ", array.toJSONString());

                        boolean post_response = processPosting(array.toJSONString(), ncard);
                        cdlogger.log("EMICARDGeneration", "process", "INFO", "post_response - ", "" + post_response);

                    } catch (IOException | NoSuchAlgorithmException | SQLException ex)
                    {
                        Logger.getLogger(EMIGenerateCards.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

            try
            {
                if (fw != null)
                {
                    fw.flush();
                    fw.close();
                }

            } catch (IOException ex)
            {
                Logger.getLogger(EMIGenerateCards.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception ex)
        {
            Logger.getLogger(EMIGenerateCards.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String sha2hash(String verify) throws NoSuchAlgorithmException
    {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] passBytes = verify.getBytes();
        byte[] passHash = sha256.digest(passBytes);
        return Translate.fromBinToHex(Translate.getString(passHash));
    }

    public boolean processPosting(String data, String cardnr)
    {
        cdlogger.log("EMICARDGeneration", "process", "INFO", "data - ", data);
        try
        {

            if (data == null)
            {
                cdlogger.log("EMICARDGeneration", "process", "INFO", "data - ", "No Data found");
                return false;
            }
            cdlogger.log("EMICARDGeneration", "process", "INFO", "Json Data to be Posted -\n", data);

            String post_response = sendmsgtoremote(data);
            cdlogger.log("EMICARDGeneration", "process", "INFO", "Post data response -\n", post_response);

            if (!post_response.equalsIgnoreCase("NOK"))
            {
                JSONParser jp = new JSONParser();
                obj = jp.parse(post_response);

                if (obj instanceof JSONObject)
                {
                    jobj = (JSONObject) obj;
                }

                String status = jobj.get("status").toString();

                if (status.equalsIgnoreCase("True") || status.equalsIgnoreCase("SUCCESS"))
                {
                    cardslist = "('" + cardnr + "')";
                    new DBHandler().updateerpposted(cardslist);
                }

            }
        } catch (ParseException ex)
        {
            Logger.getLogger(EMIGenerateCards.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public String sendmsgtoremote(String jsondata)
    {
        String jsonData = "NOK";
        try
        {

            URL url;
            URLConnection uconn;
            int timeout = 50000;

            DataOutputStream dos;
            DataInputStream dis;

            url = new URL(Helper.emierpcardposturl);
            uconn = url.openConnection();
            uconn.setRequestProperty("Content-Type", "application/json");
            uconn.setRequestProperty("charset", "utf-8");
            uconn.setReadTimeout(timeout);
            uconn.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(uconn.getOutputStream());
            out.write(jsondata);
            out.close();

            dis = new DataInputStream(uconn.getInputStream());

            StringBuilder str = new StringBuilder();
            int x;
            while ((x = dis.read()) != -1)
            {
                str.append((char) x);
            }

            jsonData = str.toString();
            System.out.println(jsonData);
            dis.close();
        } catch (MalformedURLException ex)
        {
            System.out.println("MalformedURLException - " + ex.getMessage());
        } catch (IOException ex)
        {
            System.out.println("IOException - " + ex.getMessage());
        }
        return jsonData;

    }

    private String sendMsgtoERPAPI(String bin_flag, String message, String appno)
    {
        JSONObject jobj = new JSONObject();
        JSONArray jarr = new JSONArray();
        jobj.put("strApplNo", appno);
        jobj.put("BinBatchCode", bin_flag);
        jobj.put("strApplStatus", message);
        jarr.add(jobj);
        cdlogger.log("MCARDGeneration", "sendMsgtoERPAPI", "INFO", "ERP Request", jarr.toJSONString());
        String jsonData = "NOK";
        try
        {

            URL url;
            URLConnection uconn;
            int timeout = 50000;

            DataOutputStream dos;
            DataInputStream dis;

            url = new URL(Helper.erpcardstatusurl);
            uconn = url.openConnection();
            uconn.setRequestProperty("Content-Type", "application/json");
            uconn.setRequestProperty("charset", "utf-8");
            uconn.setReadTimeout(timeout);
            uconn.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(uconn.getOutputStream());
            out.write(jarr.toJSONString());
            out.close();

            dis = new DataInputStream(uconn.getInputStream());

            StringBuilder str = new StringBuilder();
            int x;
            while ((x = dis.read()) != -1)
            {
                str.append((char) x);
            }

            jsonData = str.toString();
            System.out.println(jsonData);
            dis.close();
        } catch (MalformedURLException ex)
        {
            cdlogger.logException("MCARDGeneration", "process", ex);
            System.out.println("MalformedURLException - " + ex.getMessage());
        } catch (IOException ex)
        {
            cdlogger.logException("MCARDGeneration", "process", ex);
            System.out.println("IOException - " + ex.getMessage());
        }

        return jsonData;
    }

    /**
     *
     * @param cardNumber
     * @return
     */
    private String getNextCard(String cardNumber)
    {
        String newCard = cardNumber.substring(0, cardNumber.trim().length() - 1);
        long l_newCard = Long.parseLong(newCard);

        l_newCard++;

        newCard = Long.toString(l_newCard) + Utility.calcCheckDigit(Long.toString(l_newCard));

        //System.out.println("New Card : " + newCard);
        return newCard;
    }

    /**
     *
     * @param months
     * @return
     */
    private Date getExpiry(int months)
    {
        Date date = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months); //minus number would decrement the days

        return cal.getTime();
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        EMIGenerateCards gc = new EMIGenerateCards();
        //Helper.loadConfigProperties(args[0]);
        Helper.loadConfigprops();
        Helper.loadLogger();
        Helper.loadDBConfigurations();
        gc.process();
    }

}
